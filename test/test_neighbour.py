import pytest

from agent_war import NeighbourhoodEval
from .utils import AgentBuilder

AGENT_JEU_SAMPLE = {'agents': ['Bob', 'Dummy-1', 'Dummy-2'], 'modeMove': 'free', 'dests': {}, 'destIni': '1',
                    'destNb': 10, 'destGen': 'random', 'spawnArea': {'x': [5], 'y': [5], 'r': [2]}, 'teamNb': 0,
                    'teamColor': [[0, 0, 0]], 'resPath': 'https://jusdeliens.com/play/pytactx/resources/',
                    'bgImg': 'bg.png', 'bgColor': [255, 255, 255, 0.4], 'gridColor': [255, 255, 255, 0.4],
                    'profiles': ['default', 'attaquant', 'tank', 'saboteur', 'colline', 'arbitre'],
                    'pIcons': ['', '⚔️', '🛡️', '💨', '🌋', '👀'], 'pImgs': ['', '', '', '', '', ''],
                    'range': [0, 0, 0, 0, 0, 0], 'dtDir': [300, 300, 450, 150, 10, 10],
                    'dtMove': [300, 300, 450, 150, 10, 10], 'dtPop': [15000, 15000, 15000, 15000, 5000, 5000],
                    'dtFire': [300, 300, 300, 300, 0, 0], 'hitFire': [10, 15, 10, 8, 0, 0],
                    'hitCollision': [10, 15, 10, 8, 0, 0], 'dxMax': [1, 1, 1, 1, 1, 100], 'dyMax': [1, 1, 1, 1, 1, 100],
                    'lifeIni': [100, 75, 150, 100, 10, 10], 'ammoIni': [100, 100, 100, 100, 0, 0],
                    'invisible': [False, False, False, False, True, True],
                    'invincible': [False, False, False, False, True, True],
                    'canChat': [False, False, False, False, True, True], 'dtChat': [300, 300, 300, 300, 300, 300],
                    'gridColumns': 11, 'gridRows': 11, 'maxAgents': 20, 'dtPing': 10000, 'onlyAuthorised': False,
                    'pause': False, 'connected': True, 'nameLen': 15, 'chat': ''}
AGENT_VOISIN_SAMPLE = {
    'Dummy-1': {'x': 3, 'y': 4, 'dir': 3, 'ammo': 100, 'd': 0, 'life': 100, 'fire': False, 'led': [0, 255, 0],
                'profile': 0, 'team': 0, 'dest': '', 'eta': [], 'chat': ''},
    'Dummy-2': {'x': 5, 'y': 4, 'dir': 1, 'ammo': 100, 'd': 0, 'life': 100, 'fire': False, 'led': [0, 255, 0],
                'profile': 0, 'team': 0, 'dest': '', 'eta': [], 'chat': ''}}

AGENT_VOISIN_SAMPLE2 = {
    'dUmMy1': {'x': 3, 'y': 4, 'dir': 3, 'ammo': 100, 'd': 0, 'life': 100, 'fire': False, 'led': [0, 255, 0],
               'profile': 0, 'team': 0, 'dest': '', 'eta': [], 'chat': ''},
    'dUmMy2': {'x': 5, 'y': 3, 'dir': 0, 'ammo': 100, 'd': 0, 'life': 100, 'fire': False, 'led': [0, 255, 0],
               'profile': 0, 'team': 0, 'dest': '', 'eta': [], 'chat': ''}}


def test_dist():
    """
    test if
    :return:
    """
    # Arrange
    _agent = AgentBuilder('bob')
    _dummy1 = AgentBuilder('dummy').set_pos((10, 0))
    _dummy2 = AgentBuilder('dummy').set_pos((0, 0))
    _dummy3 = AgentBuilder('dummy').set_pos((4, 3))
    data_handler = NeighbourhoodEval(_agent, profil='safe')
    expected_dist = 10
    # Act
    actual_dist = data_handler.dist(_dummy1)
    dist2 = next(data_handler.dist(_dummy2))
    dist3 = next(data_handler.dist(_dummy3))
    # Assert
    assert expected_dist == next(actual_dist)
    assert dist2 == 0
    assert dist3 == 5


@pytest.mark.parametrize("other_agent, safe_dist_bool",
                         [
                             (AgentBuilder('dummy').set_pos((10, 0)), True),
                             (AgentBuilder('dummy').set_pos((0, 0)), False)
                         ])
def test_safe_dist(other_agent, safe_dist_bool):
    """
    Check is Agent is  far enough
    :param other_agent:
    :param safe_dist_bool:
    :return:
    """
    # Arrange
    _agent = AgentBuilder('bob')
    # Act
    eval = NeighbourhoodEval(_agent, profil='safe')
    current_safe_dist_bool = next(eval.safe_dist_bool(other_agent))
    # Assert
    assert current_safe_dist_bool is safe_dist_bool


def test_enough_ammo():
    # Arrangelife
    _agent = AgentBuilder('bob').set_ammo(50)
    _dummy1 = AgentBuilder('dummy1').set_life(30)
    data_handler = NeighbourhoodEval(agent=_agent, profil='safe')
    # Act
    can_i_attack1 = data_handler.enough_ammo(_dummy1)
    # Assert
    assert can_i_attack1


def test_not_enough_ammo():
    # Arrange
    _agent = AgentBuilder('bob').set_ammo(50)
    _dummy2 = AgentBuilder('dummy2').set_life(150)
    data_handler = NeighbourhoodEval(_agent, profil='safe')
    # Act
    can_i_attack2 = next(data_handler.enough_ammo_bool(_dummy2))
    # Assert
    assert not can_i_attack2


def test_safe_area():
    # Arrange
    _agent = AgentBuilder('bob').set_voisins(AGENT_VOISIN_SAMPLE)
    data_handler = NeighbourhoodEval(_agent, profil='safe')
    _expected = [[0, 1], [0, 0]]
    # Act
    actual_safe_are = data_handler.safe_area()
    # Assert
    assert _expected != actual_safe_are


def test_eval():
    bob = AgentBuilder('bobby')
    bobby = AgentBuilder('bob')

    assert NeighbourhoodEval(bob, profil='safe').eval(bobby)[0] == .2
    assert NeighbourhoodEval(bob, profil='safe').eval(bobby)[1] == bobby

# TODO test loop on several neighbourhood payloads
