class AgentBuilder:

    def __init__(self, name):
        self.__name = name
        self.__life = 100
        self.__ammo = 100
        self.__x, self.__y = 0, 0
        self.voisins = {}

    def set_ammo(self, _ammo):
        self.__ammo = _ammo
        return self

    def set_life(self, _life):
        self.__life = _life
        return self

    def set_pos(self, pos):
        self.__x, self.__y = pos
        return self

    def set_voisins(self, voisins):
        self.voisins = voisins
        return self

    @property
    def x(self):
        return self.__x

    @property
    def y(self):
        return self.__y

    @property
    def life(self):
        return self.__life

    @property
    def vie(self):
        return self.__life

    @property
    def ammo(self):
        return self.__ammo

    @property
    def munitions(self):
        return self.__ammo

    def __str__(self):
        return f'{self.__name} - {self.__life} - {self.__ammo} - {self.__x}:{self.__y}'
