ifeq ($(OS), Windows_NT)
	python_interpreter:=python
else
	python_interpreter:=python3
endif

debug:
	@echo $(python_interpreter)
	@echo $(OS)


test_unit:
	$(python_interpreter) -m pytest -vvvv test/


