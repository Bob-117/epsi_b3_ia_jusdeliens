import math
import pprint

from agent_race.base.pytactx import Agent


class Pythagore(Agent):

    def __init__(self, name):
        _pwd = input('pass ? ')
        self.all_targets = None
        super().__init__(id=name,
                         username="demo",
                         password=_pwd,
                         arena="pytactx",
                         server="mqtt.jusdeliens.com",
                         prompt=False,
                         verbose=False)
        self.done = False
        # self.current_target = None
        # self.reached_target = []
        # self.done = False
        # self.all_targets = [(key, value) for key, value in self.jeu['dests'].items()]
        # print(self.all_targets)

    def quandActualiser(self):
        if not self.all_targets and self.jeu:
            print('fetching targets')
            self.all_targets = [(key, (value['x'], value['y'])) for key, value in self.jeu['dests'].items()]

        # city = self.all_targets[1]
        # name, pos = city[0], city[1]
        # print(f'going to {name} - {pos} from [{self.x}.{self.y}]')
        ...

    def on_my_way(self):
        print(f'Starting my journey, pathing through {self.all_targets}')

    # def pick_one_city(self):

    def get_cities(self):
        cities = self.jeu['dests']
        for city, data in cities.items():
            yield city, (data['x'], data['y'])

    def process(self):
        while not self.done:
            if not self.all_targets:
                print('pas de target')
            elif len(self.all_targets) == 0:
                print('zero target')
                self.done = True
            else:
                self.all_targets = self.all_targets[1:5]  # TODO adapt
                all_paths = self.find_all_paths(cities=self.all_targets, current_city=self.all_targets[1])
                print('CITIES : ', self.all_targets, '\nSTARTING AT ', self.all_targets[1])
                print('PATHS : ')
                pprint.pprint(list(all_paths), indent=8)
                for city in self.all_targets:
                    print('======')
                    print(city, self.x, self.y)
                    _city_name = str(city[0])
                    _pos = city[1]
                    while self.derniereDestinationAtteinte != _city_name:
                        self.deplacerVers(_city_name)
                        self.actualiser()

                # TODO reached_target.append(city) ?
                self.done = True
            self.actualiser()

    def find_all_paths(self, cities, current_city, visited_cities=None, current_path=None, distance=0, all_paths=None):
        """
        Recursive method to find all paths in a list of cities
        Compute the distance for all visited cities
        :param cities:
        :param current_city:
        :param visited_cities:
        :param current_path:
        :param distance:
        :param all_paths:
        :return:
        """

        # Init
        if all_paths is None:
            all_paths = []
        if current_path is None:
            current_path = []
        if visited_cities is None:
            visited_cities = []

        current_path.append(current_city)
        visited_cities.append(current_city)

        # All cities have already been visited
        if len(visited_cities) == len(cities):
            all_paths.append((current_path, distance))
            return all_paths

        for next_city in cities:
            if next_city not in visited_cities:
                dist = math.sqrt(
                    (next_city[1][0] - current_city[1][0]) ** 2
                    +
                    (next_city[1][1] - current_city[1][1]) ** 2
                )
                self.find_all_paths(
                    cities=cities,
                    current_city=next_city,
                    visited_cities=visited_cities[:],
                    current_path=current_path[:],
                    distance=sum([distance, dist]),  # compute dist here
                    all_paths=all_paths
                )

        # Reset
        current_path.pop(), visited_cities.pop()

        return all_paths


if __name__ == '__main__':
    bob = Pythagore('🎸Bob6')
    bob.on_my_way()
    bob.process()
