import math

from agent_race.base.pytactx import Agent


class Pythagore(Agent):

    def __init__(self, name):
        _pwd = input('pass ? ')
        self.all_targets = None
        super().__init__(id=name,
                         username="demo",
                         password=_pwd,
                         arena="pytactx",
                         server="mqtt.jusdeliens.com",
                         prompt=False,
                         verbose=False)

    def find_all_paths(self, cities, current_city, visited_cities=None, current_path=None, distance=0, all_paths=None):
        """
        Recursive method to find all paths in a list of cities
        Compute the distance for all visited cities
        :param cities:
        :param current_city:
        :param visited_cities:
        :param current_path:
        :param distance:
        :param all_paths:
        :return:
        """

        # Init
        if all_paths is None:
            all_paths = []
        if current_path is None:
            current_path = []
        if visited_cities is None:
            visited_cities = []

        current_path.append(current_city)
        visited_cities.append(current_city)

        # All cities have already been visited
        if len(visited_cities) == len(cities):
            all_paths.append((current_path, distance))
            return all_paths

        for next_city in cities:
            if next_city not in visited_cities:
                dist = math.sqrt(
                    (next_city[1][0] - current_city[1][0]) ** 2
                    +
                    (next_city[1][1] - current_city[1][1]) ** 2
                )
                self.find_all_paths(cities, next_city, visited_cities[:], current_path[:], sum([distance, dist]),
                                    all_paths)

        # Reset
        current_path.pop(), visited_cities.pop()

        return all_paths
