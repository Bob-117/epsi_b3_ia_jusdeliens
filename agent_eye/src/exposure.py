from skimage import exposure
import matplotlib.pyplot as plt
from skimage import io


image = io.imread('../img/8.png')
fig, axs = plt.subplots(nrows=1, ncols=3, figsize=(13, 4))
canals = ['rouge', 'vert', 'bleu']
for i, canal in enumerate(canals):
    _hist = exposure.histogram(image[:, :, i])
    axs[i].plot(_hist[0])
    axs[i].set_title(canal)
# plt.savefig(f'exposure')
plt.show()
