import csv
import math
import statistics

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC

from agent_eye.base import pyscopx

# Créer la variable camera pour récupérer les images
camera = pyscopx.Camera(cameraId='CamRG67', filtreId='Bob117', server="mqtt.jusdeliens.com")


def user_menu():
    """
    user input method
    :return:
    """
    action = input("Action : ")  # train / run
    if action == 'train':
        spec_class = int(input("class : "))
        show_graph = input("show graph y/n : ")
        train(spec_class, show_graph)
    elif action == 'run':
        run()
        # data = pd.read_csv('data/dataset.csv')
        # (x, y) = (data.iloc[:, :-1].values, data.iloc[:, -1].values)
        # x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=.2)
        # classifier = SVC(kernel='sigmoid', random_state=0)
        # print(data)
        # print(x)
        # print(y)
        # classifier.fit(x_train, y_train)
        # y_pred = classifier.predict(data[1])
        # print(f'Prediction sur y {y_pred}')
        # print(f'Matrice de confusion : {confusion_matrix(data[3], y_pred)}')
        # print(f'Report : {classification_report(data[3], y_pred)}')
    else:
        print('error')
        exit()


def pixel_grad(image, pixel_x, pixel_y):  # TODO lirePixelLuminosite en params
    # regarder doxytags + wizzard
    """
    Gradient de luminiosité pour un pixel donné
    :param image:
    :param pixel_x:
    :param pixel_y:
    :return:
    """
    return math.sqrt(
        (image.lirePixelLuminosite(pixel_x + 1, pixel_y) - image.lirePixelLuminosite(pixel_x - 1, pixel_y)) ** 2
        +
        (image.lirePixelLuminosite(pixel_x, pixel_y + 1) - image.lirePixelLuminosite(pixel_x, pixel_y - 1)) ** 2
    )


def generate_data_image(camera, graph=False):
    """
    Entry : camera
    Output: 2x2 matrix topL, botL, topR, botR with avg gradient for every image quarter
    :param camera:
    :return:
    """
    main_matrix = []  # x*y matrix with all gradient for all pixel
    horizontal_matrix = []  # 1*y matrix with column gradient
    topL, botL, topR, botR = [], [], [], []  # 2*2 matrix, splitting image gradient by quarter
    for x in range(2, camera.largeur - 1, 1):
        current_x_array = []
        for y in range(2, camera.hauteur - 1, 1):
            current_gradient = pixel_grad(camera, x, y)

            if x <= camera.largeur / 2 and y <= camera.hauteur / 2:
                topL.append(current_gradient)
            elif x <= camera.largeur / 2 and y > camera.hauteur / 2:
                botL.append(current_gradient)
            elif x > camera.largeur / 2 and y <= camera.hauteur / 2:
                topR.append(current_gradient)
            else:
                botR.append(current_gradient)

            # current_gradient = current_gradient / 2
            current_x_array.append(current_gradient)
        main_matrix.append(current_x_array)
        horizontal_matrix.append(statistics.mean(current_x_array))
    # avg_grad = statistics.mean(main_matrix)
    # print(avg_grad)
    try:
        quarter_gradient_matrix = (
            [statistics.mean(topL), statistics.mean(topR), statistics.mean(botL), statistics.mean(botR)]
        )
        # if spec_class:
        #     quarter_gradient_matrix.append(spec_class)
        # with open('data/dataset.csv', 'a') as datafile:
        #     writer = csv.writer(datafile)
        #     writer.writerow(quarter_gradient_matrix)
    except statistics.StatisticsError:
        print('nope')
        quarter_gradient_matrix = [0, 0, 0, 0]

    if graph:
        # plt.title("main graph")
        # plt.plot(main_matrix, color="red")
        # plt.show()

        # a = np.array((1, 2, 3))
        # x1, x2 = np.meshgrid(a, a)
        # X1, X2 = np.meshgrid(x1, x2)
        # Y = np.sqrt(np.square(X1) + np.square(X2))
        # cm = plt.cm.get_cmap('viridis')
        # plt.scatter(X1, X2, c=Y, cmap=cm)
        # plt.show()

        # for (i, j), z in np.ndenumerate(np.array(main_matrix)):
        #     print(i, j, main_matrix[i, j])
        arr2d = [[1, 2, 5, 3], [2, 1, 2, 5], [5, 3, 4, 4], [5, 5, 3, 4]]

        # figure, subgraph = plt.subplots(2, 2)
        # subgraph[0, 0].matshow(A=main_matrix, fignum=1)

        plt.matshow(A=main_matrix, fignum=0)
        plt.show()
        # fig = plt.figure()
        # plt.xlabel('left -------------- right')
        # plt.ylabel('top bot', rotation=0)
        # plt.title("Left Right graph")
        # plt.plot(horizontal_matrix, color="red")
        # plt.show()

    return quarter_gradient_matrix


def train(spec_class, show_graph):
    """
    entrainer le modele avec des variables explicatives connues
    et des output connus
    ici des frames du robot en entrée, enconnaissant la presence ou l'absence d'obstacle
    :param show_graph: montrer le graph plt ou non
    :param spec_class: la classe connue
    """
    print('TRAIN')
    while True:  # TODO changer et cliquer a chaque nouvelle image + classe
        # Récupère l'image de la caméra
        camera.actualiser()
        if camera.estConnecte:
            # print(camera)
            # Affiche la taille de l'image reçue
            print("Training image : (" + str(camera.largeur) + ", " + str(camera.hauteur) + ")")
            # Pour chaque pixel de l'image
            grad_matrix = generate_data_image(camera=camera, graph=show_graph)
            if grad_matrix != [0, 0, 0, 0]:
                grad_matrix.append(spec_class)
                with open('data/dataset.csv', 'a') as datafile:
                    writer = csv.writer(datafile)
                    writer.writerow(grad_matrix)
            # IA.fit()


def run():
    """
    lance le programme, initialise un apprentissage a partir du dataset
    (dataset generé dans la method train())
    puis on traite les images a direct
    :return:
    """
    data = pd.read_csv('data/dataset.csv')
    print(data)
    (x, y) = (data.iloc[:, :-1].values, data.iloc[:, -1].values)
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=.2)
    classifier = SVC(kernel='linear', random_state=0)
    print(data)
    print(x)
    print(y)
    classifier.fit(x_train, y_train)
    y_pred = classifier.predict(x_test)
    print(f'Prediction sur y {y_pred}')
    print(f'Matrice de confusion : {confusion_matrix(y_test, y_pred)}')
    print(f'Report : {classification_report(y_test, y_pred)}')

    # while True:
    #     # Récupère l'image de la caméra
    #     camera.actualiser()
    #
    #     if camera.estConnecte:
    #         # print(camera)
    #         # Affiche la taille de l'image reçue
    #         print("Taille image en cours : (" + str(camera.largeur) + ", " + str(camera.hauteur) + ")")
    #
    #         # Pour chaque pixel de l'image en cours
    #         grad_matrix = handle_image(camera=camera)
    #         print(f'l\'ia va traiter {grad_matrix}')
    #         y_pred = classifier.predict(grad_matrix)
    #         print(f'switch case pas d\'obstacle/obstacle/robot : {y_pred}')

    # TODO CORRECTION OREL
    # while True:
    #     camera.actualiser()
    #     if camera.estConnecte:
    #         avg_grad = eval_average_gradient(camera.lirePixelLuminosite, camera.largeur, camera.hauteur)
    #         print(f"Average gradient = {avg_grad}")
    #         sd_grad = eval_standard_deviation_grad(camera.lirePixelLuminosite, camera.largeur, camera.hauteur, avg_grad)
    #         print(f"Stan. deviation grad. = {sd_grad}")
    #         y_pred = classifier.predict(
    #             array(characterize(camera.lirePixelLuminosite, camera.largeur, camera.hauteur)).reshape(1, -1))
    #         if y_pred[0] == 1:
    #             print("OBSTACLE DETECTED")
    #         else:
    #             print("CLEAR ! ;)")
    output_dict = {
        0: 'clear',
        1: 'topleft',
        2: 'topright',
        3: 'botleft',
        4: 'botright'
    }
    print(output_dict[y_pred])


user_menu()
