import math


def pixel_grad(image, pixel_x, pixel_y):  # TODO lirePixelLuminosite en params
    # regarder doxytags + wizzard
    """
    Gradient de luminiosité pour un pixel donné
    :param image:
    :param pixel_x:
    :param pixel_y:
    :return:
    """
    return math.sqrt(
        (image.lirePixelLuminosite(pixel_x + 1, pixel_y) - image.lirePixelLuminosite(pixel_x - 1, pixel_y)) ** 2
        +
        (image.lirePixelLuminosite(pixel_x, pixel_y + 1) - image.lirePixelLuminosite(pixel_x, pixel_y - 1)) ** 2
    )

