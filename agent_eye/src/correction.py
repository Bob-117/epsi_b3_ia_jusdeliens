# Import les libs nécessaires (sklearn ...)
import math


def grad(L, x, y):
    """Pour calculer le gradient de luminosité autour du point x,y. L est la fonction qui renvoie la luminosité d'un point x,y sur une image"""
    return math.sqrt(
        (L(x + 1, y) - L(x - 1, y)) ** 2
        +
        (L(x, y + 1) - L(x, y - 1)) ** 2
    )


def characterize(L):
    """Calcule et renvoi dans un array les variables explicative pour l'image donnée.  L est la fonction qui renvoie la luminosité d'un point x,y sur cette image"""
    ...


# Calculer la moyenne des gradients de tous les points de l'image
# Calculer l'écart-type des gradients de tous les points de l'image
# Renvoyer l'array des variables explicatives


##########################################
# 1. ENTRAINEMENT
# Créer dataset contenant toutes les images de test et entrainement

# Initaliser le dataset de test et d'entrainement à partir du fichier csv
with open("dataset/data.csv") as fp:
    pass

# Si entrainement pas encore effectué (dataset vide)
# Demander en boucle
# Si l'utilisateur veut encore capturer des images d'entrainement ou de test
# capturer image de la caméra
# demander la classe à l'utilisateur (obstacle ou pas obstacle)
# Calculer les variables explicatives
#	rajouter dans le dataset

# Fabrication des données d'entrainement à partir du dataset (liste de variables explicatives et classes correspondantes déjà connues)
x_train = []
y_train = []

# Fabrication des données de tests à partir du dataset (liste de variables à explicatives et classes correspondantes déjà connues)
x_test = []
y_test = []

# Entrainement du classifier à partir des données d'entrainement
classifier = None  # choisir ici le type de classifier avec ses hyperparamètres
classifier.fit(x_train, y_train)

# Génération de la matrice de confusion pour vérifier la bonne précision de notre modèle


##########################################
# 2. PREDICTION (dans la boucle principale)
# Récupérer la dernière image de la caméra

# Calculer les variables explicatives (appel de la fonction characterize)

# On demande au classifier de prédire la classe de l'image (obstacle ou pas d'obstacle) en passant les 2 variables explicatives (moyenne gradient et ecart-type gradient)

# On affiche la prédiction dans la console
