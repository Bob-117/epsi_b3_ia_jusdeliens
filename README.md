<div align="right">

![](readme_assets/made-with-python.svg)

</div>

<h1> Module IA & Framework </h1>

    Ce projet est réalisé dans le cadre d'un module de cours IA & Framework dispensé par Julien Arné de l'entreprise JusDeLiens.
    La librairie Pytactx, module d'interaction avec l'environnement JusDeLiens, est sous license CC BY-NC-ND 3.0.
    Ainsi ce projet l'est également (plus d'informations sur https://creativecommons.org/licenses/by-nc-nd/3.0/ ).

## JusDeLiens

Julien Arné nous emmène dans l'univers Pytactx où nous y rencontrons ses agents, les robots OVA.
Ces robots sont équipés d'une caméra, de LED et d'un buzzer, avec lesquels nous pouvons interagir grace aux méthodes de
la lib Pytactx.

Au travers de multiples défis rencontrés lors de ce voyage, nous découvrirons le monde de l'intelligence artificielle.
Au dela de l'aspect technique et code, les concepts, méthodes scientifiques & bonnes pratiques sont parties intégrantes
de ce module de cours.

Les différents choix techniques et méthodes scientifiques 
pour effectuer le traitement des données sont discutées et échangées en groupe, mais le choix revient à l'apprenant. 
Ainsi `import math` & `import numpy` sont récurrents sur ce repo :^). La visualisation se fait avec `matplotlib` principalement.


    Note : Les codes sources de EYE, WAR et RACE sont peut etre encore en migration depuis divers autres depots (certaines 
    méthodes concrètes sont deja présentes).
    Seul le code COLOR, necessaire à l'évaluation, est présent.


<ins> Environnement général :</ins>

- Un robot OVA
- Un IDE et python 3.10 ou 3.11
- La lib Pytactx fournie par JusDeLiens

- Un serveur distant et un broker pour orchestrer tout cet univers
- Un plateau écran de jeu

<ins> Librairies python utilisées :</ins>

- Python 3.10
- Numpy
- Pandas
- Scikit-learning
- Matplotlib



## Sommaire

1. [EYE](#i-eye) (analyse d'image)
2. [WAR](#ii-war) (comportement du robot OVA)
3. [RACE](#iii-race) (analyse de métriques et algo)
4. [COLOR](#iv-color) (analyse d'image, évaluation)


## I) EYE

### <ins>Contexte :</ins>

Les robots OVA ont perdu la vue et ne peuvent plus se repérer dans leurs déplacements.
À l'aide de méthodes de traitement d'images et d'algorithmes de machine learning supervisé, notre but est dans un
premier temps de détecter un obstacle, puis faire la différence entre un obstacle quelconque et un autre robot OVA.

### <ins>Problématique :</ins>

    Redonner la vue à OVA

### <ins>Cahier des charges :</ins>

- Récupérer les images de la camera d'OVA
- Réfléchir à des features pertinentes pour entrainer un modèle SVM
- Extraire ces features
- Entrainer, tester et valider notre modèle
- Deployer notre modèle entrainé afin de rendre la vue aux robots OVA

### <ins>Développement :</ins>

#### État initial

````python
    agent.get_pixel_rgb()
````

#### Solutions développées

  - Gradient de luminosité
  - SVM

<details>
<summary>Code : Calcul de gradient</summary>

```python
import math

def pixel_grad(image, pixel_x, pixel_y):  # TODO lirePixelLuminosite en params
    """
    Gradient de luminiosité pour un pixel donné
    :param image:
    :param pixel_x:
    :param pixel_y:
    :return:
    """
    return math.sqrt(
        (image.lirePixelLuminosite(pixel_x + 1, pixel_y) - image.lirePixelLuminosite(pixel_x - 1, pixel_y)) ** 2
        +
        (image.lirePixelLuminosite(pixel_x, pixel_y + 1) - image.lirePixelLuminosite(pixel_x, pixel_y - 1)) ** 2
    )
```
</details>

#### État final & démonstration

<div align="center">

Les robots OVA reconnaitront elles leur maitre en gradient de luminosité ?
![](readme_assets/gradient_1.png)                                           

</div>


| Image `agent_eye/src/exposure.py`                                                                                       | Resultat                                                       |
|-------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------|
| ![](agent_eye/img/8.png)                                                                                                | ![](agent_eye/img/exposure.png)                                |
| An algorithm for local contrast enhancement,<br/>that uses histograms computed over different tile regions of the image | https://scikit-image.org/docs/stable/api/skimage.exposure.html |




### <ins>Retrospective :</ins>

- [ ] ce que j'ai compris
- [ ] ce que je n'ai pas compris
- [ ] les évolutions possibles

## II) WAR

### <ins>Contexte :</ins>

Maintenant que les robots OVA ont retrouvé la vue, elles vont pouvoir partir en guerre pour défendre leur territoire.
Ainsi, se déplaçant sur une grille en (x, y), nous récupérons des données de jeu, & concernant de potentiels voisins sur
la grille (des points de vie, un nombre de munitions...).


<div align="center">

![](agent_war/assets/dummy_agent.gif)

`agent_war/src/populate.py`
</div>

### <ins>Problématique :</ins>

    Donner un comportement situationnel à OVA

### <ins>Cahier des charges :</ins>

- Récupérer les données de `jeu` & `voisins`
- Prélever des métriques pertinentes
- Implémenter une classe python `Soldier` qui hérite de `pytactx.Agent` et qui suit le design pattern `state machine`
- Implémenter des algorithmes de comportement
- Faire varier le comportement de notre OVA (Chercher plus de munitions, fuir les voisins dangereux...)
- Partir à la recherche d'une cible ou fuir

### <ins>Développement :</ins>

#### État initial

```py
    print(agent.voisins)
```

#### Solutions développées

  - State machine 
  - Traitement des données 

<br>

<i>Simulation avec Pygame :</i>

<div align="center">

![](agent_war/assets/huho.gif)

</div>
<details>
<summary>Code : Récupération de métriques (coordonnées, munitions)</summary>

```py
from pytactx import Agent

if __name__ == '__main__':
    bob = Agent()
    jeu = bob.jeu
    print(jeu)

```

```json
[
  {
    "agents": [
      "Orel'",
      "bob"
    ],
    "modeMove": "free",
    "dests": {},
    "destIni": "1",
    "destNb": 10,
    "destGen": "random",
    "spawnArea": {"x": [5],"y": [5],"r": [2]},
    "teamNb": 0,
    "teamColor": [[0,0,0]],
    "resPath": "https://jusdeliens.com/play/pytactx/resources/",
    "bgImg": "bg.png",
    "bgColor": [255,255,255, 0.4],
    "gridColor": [255, 255, 255, 0.4],
    "profiles": ["default","attaquant","tank","saboteur","colline","arbitre"],
    "pIcons": ["","⚔️","🛡️","💨","🌋","👀"],
    "dtDir": [300, 300, 450, 150, 10, 10],
    "dtMove": [300, 300, 450, 150, 10, 10],
    "dtPop": [15000, 15000, 15000, 15000, 5000, 5000],
    "dtFire": [ 300,300, 300,300,0,0],
    "hitFire": [ 10, 15, 10,8,0, 0],
    "hitCollision": [10,15,10, 8,0, 0 ],
    "dxMax": [ 1,1, 1,1,1, 100],
    "dyMax": [1, 1, 1, 1, 1, 100],
    "lifeIni": [100,75, 150, 100, 10,10 ],
    "ammoIni": [ 100, 100,100, 100, 0, 0 ],
    "invisible": ["False","False","False","False","True","True"],
    "invincible": ["False","False","False","False","True","True"],
    "canChat": ["False","False","False","False","True","True"],
    "dtChat": [300,300,300,300, 300,300],
    "gridColumns": 11,
    "gridRows": 11,
    "maxAgents": 20,
    "dtPing": 10000,
    "onlyAuthorised": "False",
    "pause": "False",
    "connected": "True",
    "nameLen": 15,
    "chat": ""
  }
]
```

```py
from pytactx import Agent

if __name__ == '__main__':
    bob = Agent(name='Bobby')
    jeu = bob.jeu
    print(jeu)
```

```json
[
  {
    "Orel": 
    {
      "x": 5, "y": 6, "dir": 1, "ammo": 100, "d": 1, "life": 100, "fire": "False",
      "led": [0, 255, 0], "profile": 0, "team": 0, "dest": "", "eta": [], "chat": ""
    }
  },
  {
    "Bob": 
    {
      "x": 11, "y": 7, "dir": 1, "ammo": 100, "d": 1, "life": 100, "fire": "False",
      "led": [0, 255, 0], "profile": 0, "team": 0, "dest": "", "eta": [], "chat": ""
    }
  }
]
```

Récupérer la distance et évaluer une distance de sécurité, en pondérant cette métrique `self.__weights.DIST.value` afin
de tester différents comportements :

```python
import math
from pytactx import Agent


class Soldier(Agent):
    def dist(self, _other):
        """
        dist between self.__agent and other agent
        :param _other:
        :return:
        """
        yield math.sqrt(
            (self.__agent.x - _other.x) ** 2
            +
            (self.__agent.y - _other.y) ** 2
        )

    def safe_dist(self, _other, _safe_dist_ref=10):
        """
        Return dist is safe between self.__agent and other agent 
        :param _other: other agent
        :param _safe_dist_ref: safe distance
        :return: agent is in a safe spot regarding distance (0, .5, 1)
        """

        yield 1 if next(self.dist(_other)) > _safe_dist_ref else (
            .5 if 5 < next(self.dist(_other)) < _safe_dist_ref else 0), self.__weights.DIST.value


```
</details>


#### État final & démonstration

    
    Le robot change sa LED de couleur vert/rouge en fonction de son état

### <ins>Retrospective :</ins>

- [ ] ce que j'ai compris

```txt
Design pattern State Machine : être != avoir un/des état(s)
```

- [ ] ce que je n'ai pas compris
- [ ] les évolutions possibles

## III) RACE

### <ins>Contexte :</ins>

Nos robots OVA souhaitent nous faire visiter leur pays en passant par différentes villes. Mais l'itinéraire n'est pas
encore décidé et le choix nous revient.
À partir des positions (x, y) d'une liste de ville, nous devons implémenter et réaliser un benchmark de différentes
méthodes (Brute force, A*...).

### <ins>Problématique :</ins>

    Passer par l'ensemble des villes, en un temps minimum de trajet ... et de calcul !

### <ins>Cahier des charges :</ins>

- Récupérer les coordonnées des villes
- Implémenter un algo brute force
- Trouver un moyen de l'optimiser


### <ins>Développement :</ins>

#### État initial

```python
  print(agent.villes)
```
    

#### Solutions développées

  - Mathématiques : permutations
  - Métriques = distances + pondération
  - Brute force
  - Dijkstra
  - A*
  - comparaison matplotlib
  - Comparaison à des modeles ML
  - Présentation en cours du machine learning par renforcement (10min)


<details>
<summary>Code : Methode récursive pour trouver tous les chemins possibles</summary>

```python

import math
def find_all_paths(self, cities, current_city, visited_cities=None, current_path=None, distance=0, all_paths=None):
    """
    Find all paths recursively
    Sum the distance each loop
    """
    if all_paths is None:
        all_paths = []
    if current_path is None:
        current_path = []
    if visited_cities is None:
        visited_cities = []

    current_path.append(current_city)
    visited_cities.append(current_city)

    if len(visited_cities) == len(cities):
        all_paths.append((current_path, distance))
        return all_paths

    for next_city in cities:
        if next_city not in visited_cities:
            dist = math.sqrt(
                (next_city[1][0] - current_city[1][0]) ** 2
                +
                (next_city[1][1] - current_city[1][1]) ** 2
            )
            self.find_all_paths(cities, next_city, visited_cities[:], current_path[:], sum([distance, dist]),
                                all_paths)

    # Reset
    current_path.pop(), visited_cities.pop()

    return all_paths
```

</details>

#### État final & démonstration
    
```shell
CITIES :  [('1', (12, 29)), ('2', (21, 6)), ('3', (0, 1)), ('4', (5, 0))] 
STARTING AT  ('2', (21, 6))
PATHS : 
[       (       [('2', (21, 6)), ('1', (12, 29)), ('3', (0, 1)), ('4', (5, 0))],
                60.260290007505354),
        (       [('2', (21, 6)), ('1', (12, 29)), ('4', (5, 0)), ('3', (0, 1))],
                59.63006536440232),
        (       [('2', (21, 6)), ('3', (0, 1)), ('1', (12, 29)), ('4', (5, 0))],
                81.88299334873113),
        (       [('2', (21, 6)), ('3', (0, 1)), ('4', (5, 0)), ('1', (12, 29))],
                56.51892043886828),
        (       [('2', (21, 6)), ('4', (5, 0)), ('1', (12, 29)), ('3', (0, 1))],
                77.3839676944433),
        (       [('2', (21, 6)), ('4', (5, 0)), ('3', (0, 1)), ('1', (12, 29))],
                52.65011942768348)]

```


### <ins>Retrospective :</ins>

```txt
    ...
```

- [ ] ce que j'ai compris
- [ ] ce que je n'ai pas compris
- [ ] les évolutions possibles

## IV) COLOR

### <ins>Contexte :</ins>

Place au spectacle de fin de voyage, un son et lumière !
Des couleurs vont apparaitre sur l'écran, les robots OVA doivent les reconnaitre et jouer le son associé.

### <ins>Problématique :</ins>

    Exploiter les images des robots OVA afin d'entrainer un modèle SVM de reconnaissance de couleur 

### <ins>Cahier des charges :</ins>

- Récupérer les images de la camera d'OVA
- Mettre en place une campagne de collecte de données
- Relever des features pertinentes
- Creer un dataset
- Entrainer un modèle SVM
- Tester ce modèle

Machine learning supervisé :

![](readme_assets/supervised.svg)


### <ins>Développement :</ins>

####  État initial

 -  Métriques à étudier :
     - Gradient
     - Scintillement
     - RGB
     - Dimension
     - Flou
     - HSV


<details>
<summary>Code : Recuperation des images </summary>

````python
from agent_color.base.ova import OvaClientMqtt

class Picasso(OvaClientMqtt):

    def __init__(self, ova_id, _username):
        self.current_img = None
        _pwd = input('pass ? ')
        super().__init__(id=ova_id,
                         username=_username,
                         password=_pwd,
                         arena="ishihara",
                         server="xxx",
                         imgOutputPath=None)
    
        
   def handle_one_frame(self):
          """
          Get one real picture
          Return pixels matrix like
          [
              [[r, g, b],[r, g, b],[r, g, b]],
              [[r, g, b],[r, g, b],[r, g, b]],
              [[r, g, b],[r, g, b],[r, g, b]],
          ]
          """
          if self.getImageWidth() == 0 or self.getImageHeight() == 0:
              print('ERROR FETCHING IMAGES')
              exit(117)
  
          width, height = self.getImageWidth(), self.getImageHeight()
          px_rgb = (
              (
                  list(
                      map(lambda rgb: rgb, self.getImagePixelRGB(x, y))
                  )
                  for x in range(width))
              for y in range(height)
          )
          self.current_img = px_rgb
          return px_rgb

````
</details>


#### Solutions développées


- [X] Clustering des couleurs dominantes

Methode d'Elbow pour trouver le nombre optimal de cluster

Pour une valeur `n` de 1 a N, on fait des groupes de pixels. 
On mesure ensuite la dispersion pour chaque `n`, puis on évalue le nombre optimal `n*` de cluster de pixels.
(A partir de x, la dispersion ne diminue plus, pas besoin de creer plus de cluster)

<details>
<summary>Code : Méthode d'Elbow </summary>

```python
import numpy as np
from matplotlib import pyplot as plt
from sklearn.cluster import KMeans
from kneed import KneeLocator
from PIL import Image
from collections import Counter


def one_frame_to_csv_line(frame, target, src, _n):
    """
    EXTRACT FEATURE HERE, FORMAT ROW TO CSV
    """
    dominant_colors = elbow_method_colors_clustering(frame, target, src, _n)
    # print(f'CSV ROW : {dominant_colors}')

    with open(f'dataset/{src}.csv', 'a+') as final_dataset:
        color = dominant_colors[1]
        r1, g1, b1 = color[0], color[1], color[2]
        final_dataset.write(f'{r1},{g1},{b1},{target}\n')

def elbow_method_colors_clustering(frame, target, src, _n):
    # Load the image
    img = Image.open(frame)
    img = img.convert('RGB')

    # Convert image to nparray
    img_array = np.array(img)

    # Flatten the array : rows of pixels
    rows, cols, ch = img_array.shape
    pixels = img_array.reshape(rows * cols, ch)

    # TODO params ?
    cluster_max = 12

    # Elbow method to find the best number of clusters
    disp = []
    for k in range(1, cluster_max):
        # TODO stop before warning
        kmeans = KMeans(n_clusters=k, random_state=0, n_init='auto').fit(pixels)
        disp.append(kmeans.inertia_)

    # ## PLOT ELBOW
    plt.plot(range(1, cluster_max), disp)
    plt.title('Elbow Method')
    plt.xlabel('Number of clusters')
    plt.ylabel('Inertia | Dispersion')
    plt.show()

    # Find the optimal number of clusters
    kn = KneeLocator(range(1, cluster_max), disp, curve='convex', direction='decreasing')
    opti_n_clusters = kn.knee
    print('N* : ', opti_n_clusters)

    # Perform k-means clustering with the best number of clusters
    kmeans = KMeans(n_clusters=opti_n_clusters, random_state=0, n_init='auto').fit(pixels)

    # Get the dominant colors & frequencies
    labels = kmeans.labels_
    dominant_colors = kmeans.cluster_centers_

    # Normalize colors [0, 1]
    dominant_colors_norm = dominant_colors / 255
    # print('DOMINANT COLORS : ', dominant_colors_norm)

    # HANDLING SOME 1.000000005856 ????? fck u
    norm_dom_color = list(
        map(
            lambda row: list(
                map(lambda value: 1 if value > 1 else value,
                    row
                    )
            ), dominant_colors_norm)
    )

    color_counts = Counter(labels)

    print('DOMINANT COLORS : ', dominant_colors.astype(int))

    # Dominant colors bar plot
    plt.bar(range(len(color_counts)), color_counts.values(), color=norm_dom_color)
    plt.xticks(range(len(color_counts)), dominant_colors.astype(int))
    plt.xlabel('Dominant Colors')
    plt.ylabel('Frequency')
    # plt.savefig(f'../readme_assets/histo/{src}/{target}_{_n}.png')
    plt.show()

    return dominant_colors.astype(int)
```
</details>

Premiers résultats de l'algo :

##### 2 couleurs

|                    Image d'origine                     |                         Elbow                         | Clustering                                            |
|:------------------------------------------------------:|:-----------------------------------------------------:|-------------------------------------------------------|
| ![](readme_assets/ova_colors/damier/damier_resize.png) | ![](readme_assets/ova_colors/damier/elbow_damier.png) | ![](readme_assets/ova_colors/damier/histo_damier.png) |


```shell
OPTI_NB_CLUSTER :  2
DOMINANT COLORS :  [[  0   0   0]
                    [255 255 255]]

```

##### 4 couleurs

|                 Image d'origine                 |                          Elbow                           | Clustering                                               |
|:-----------------------------------------------:|:--------------------------------------------------------:|----------------------------------------------------------|
| ![](readme_assets/ova_colors/damier4/img_1.png) | ![](readme_assets/ova_colors/damier4/elbow_damier_4.png) | ![](readme_assets/ova_colors/damier4/histo_damier_4.png) |

```shell
OPTI_NB_CLUSTER :  4
DOMINANT COLORS :   [[242  58  65]
                     [ 78 175  83]
                     [253 235  64]
                     [ 36 149 239]]
```

##### Frame OVA

|                 Image d'origine                 |                          Elbow                           | Clustering                                               |
|:-----------------------------------------------:|:--------------------------------------------------------:|----------------------------------------------------------|
| ![](readme_assets/ova_colors/ova/new_image.png) | ![](readme_assets/ova_colors/ova/elbow_first_purple.png) | ![](readme_assets/ova_colors/ova/histo_first_purple.png) |

```shell
OPTI_NB_CLUSTER :  3
DOMINANT COLORS :   [[ 33  27  31]
                     [132 141 119]
                     [ 74  65 110]]
```

À partir de ces métriques, et dans le contexte où nous connaissons la liste exhaustive des couleurs, nous pouvons envisager la creation d'un dataset tel que :

```csv
color1,color2,color3,color4,target
[ 33  27  31],[132 141 119],[ 74  65 110], [-1, -1, -1],purple
[ 33  27  31],[132 141 119],[ 55  18 180], [117, 117, 117],blue
```
*Nous veillerons à normaliser ces données avant traitement, elles ne sont pas exploitables en l'état, cet exemple sert à illustrer la logique employée.*

Même si c'est en bonne voie, nous remarquerons à l'œil humain que la précision et la justesse laissent à désirer.
Cependant, nous pouvons deja entrainer un modèle SVM et le tester.
Si besoin nous pourrons tenter de filtrer les images afin d'écarter des couleurs trop proches de `[0, 0, 0]` ou de `[255, 255, 255]`.

Mise à jour :

Je remercie mes collègues @collintom3, @Sullfurick & @Mistayan 
sans qui les rares données en ma possession n'auraient pas suffi à élaborer un dataset suffisant, autant en quantité qu'en qualité. 

Exploitation de ces données :

```txt
agent_color/src/frame_process.py
```

<details>
<summary>Code : Generation des 3 datasets </summary>

```python
for _ in ['tom', 'stephen', 'orel']:
    frame_process.loop_on_pictures_from_src(src=_)
```

```python
def loop_on_pictures_from_src(src):
    """
    Loop on several pictures file
    Ine the directory dataset/data/data_{src}/captured
    e.g. dataset/data/data_tom/captured/blue/1.png
    Extract mandatory features & write it in a csv file to train a model
    """
    captured_directory = f'dataset/data/data_{src}/captured'
    print(f'SRC : {src}')
    for subdir, dirs, files in os.walk(captured_directory):
        if files:
            target = os.path.basename(subdir)
            count = 0
            for file in files:
                count += 1
                print(f'>>> {src} >>> {target} >>> {file}')
                if count < 200:
                    one_frame_to_csv_line(os.path.join(subdir, file), target=target, _n=count, src=src)

```

</details> 


Application de notre algorithme aux datasets :

| DATASET DE TOM |                                            |                                            |                                            |                                            |                                            |
|----------------|--------------------------------------------|--------------------------------------------|--------------------------------------------|--------------------------------------------|--------------------------------------------|
| BLUE           | ![](readme_assets/histo/tom/blue_1.png)    | ![](readme_assets/histo/tom/blue_2.png)    | ![](readme_assets/histo/tom/blue_3.png)    | ![](readme_assets/histo/tom/blue_4.png)    | ![](readme_assets/histo/tom/blue_5.png)    |
| RED            | ![](readme_assets/histo/tom/red_1.png)     | ![](readme_assets/histo/tom/red_2.png)     | ![](readme_assets/histo/tom/red_3.png)     | ![](readme_assets/histo/tom/red_4.png)     | ![](readme_assets/histo/tom/red_5.png)     |
| CYAN           | ![](readme_assets/histo/tom/cyan_1.png)    | ![](readme_assets/histo/tom/cyan_2.png)    | ![](readme_assets/histo/tom/cyan_3.png)    | ![](readme_assets/histo/tom/cyan_4.png)    | ![](readme_assets/histo/tom/cyan_5.png)    |
| MAGENTA        | ![](readme_assets/histo/tom/magenta_1.png) | ![](readme_assets/histo/tom/magenta_2.png) | ![](readme_assets/histo/tom/magenta_3.png) | ![](readme_assets/histo/tom/magenta_4.png) | ![](readme_assets/histo/tom/magenta_5.png) |
| YELLOW         | ![](readme_assets/histo/tom/yellow_1.png)  | ![](readme_assets/histo/tom/yellow_2.png)  | ![](readme_assets/histo/tom/yellow_3.png)  | ![](readme_assets/histo/tom/yellow_4.png)  | ![](readme_assets/histo/tom/yellow_5.png)  |
| GREEN          | ![](readme_assets/histo/tom/green_1.png)   | ![](readme_assets/histo/tom/green_2.png)   | ![](readme_assets/histo/tom/green_3.png)   | ![](readme_assets/histo/tom/green_4.png)   | ![](readme_assets/histo/tom/green_5.png)   |


Observations :

- La couleur principale trouvée est généralement un noir `[20, 20, 20]`, liée au contexte de capture des images (faible luminosité). 
- Est-ce pertinent de la garder ?
- Est-ce pertinent de récupérer une nouvelle série d'image plus lumineuse ?

De plus certaines images donnent des résultats erronés et la cause (algo, qualité de l'image) est pour le moment inconnue :


| Couleur | Résultats                                                                                           | Observations                                                       |
|---------|-----------------------------------------------------------------------------------------------------|--------------------------------------------------------------------|
| YELLOW  | ![](readme_assets/histo/bugged/bugged_yellow.png)                                                   | Seulement 2 clusters sur certaines images                          |
| CYAN    | ![](readme_assets/histo/bugged/bugged_cyan_1.png) ![](readme_assets/histo/bugged/bugged_cyan_2.png) | Les couleurs dominantes 2 et 3 présentent un diagramme particulier |
| BLUE    | ![](readme_assets/histo/bugged/bugged_blue.png)                                                     | Si on laisse matplolib faire n'importe quoi..                      |



Nous prouvons donc que la visualisation des données est primordiale, ainsi que la remise en questions régulière de nos méthodes de travail et biais potentiels.

- [X] Préparation de notre dataset

Nous choisissons de selectionner la 2e couleur dominante comme feature d'entrainement.

<details>
<summary>Code : Extraction de la 2e couleur dominante</summary>

```python
def one_frame_to_csv_line(frame, target, src, _n):
    """
    EXTRACT FEATURE HERE, FORMAT ROW TO CSV
    """
    dominant_colors = elbow_method_colors_clustering(frame, target, src, _n)

    with open(f'dataset/{src}.csv', 'a+') as final_dataset:
        color = dominant_colors[1]
        r1, g1, b1 = color[0], color[1], color[2]
        final_dataset.write(f'{r1},{g1},{b1},{target}\n')
```
</details>

```csv
dominant_color_r,dominant_color_g,dominant_color_b,target
71,51,43,red
71,51,44,red
93,102,63,red
85,119,67,red
94,122,93,cyan
81,127,98,cyan
74,111,98,cyan
90,120,71,green
89,119,70,green
```


- [X] Visualisation de nos données

```txt
agent_color/src/data_visualization.py
agent_color/gif/gif.py
```


| Dataset | Détails                                                                | Diagramme                                 |
|---------|------------------------------------------------------------------------|-------------------------------------------|
| Tom     | Répartition en nuage de points des individus du dataset                | ![](readme_assets/data_visualization.gif) |
| Orel    | Repartition en histogramme des différentes target d'un dataset         | ![](readme_assets/histo_dataset.png)      |
| Stephen | Moyenne en histogramme des composante R, G,& B pour chacune des target | ![](readme_assets/rbg_over_target.png)    |

Par manque de temps, nous ne nous occuperons pas de la normalisation des données (entre 0 et 1, passer les couleurs nominales 'red' a des valeurs cardinales (sans hiérarchie, qui peut biaiser l'entrainement))

- [X] Entrainement et validation de notre modèle

<details>
<summary>Code : Modele SVM</summary>

```txt
Voir le fichier agent_color/src/svm_model.py
```

</details>


Validation de notre modèle :

| Dataset | Paramètres                                                                                                                                                                                                               | Matrice de confusion                   | Métriques                                       |
|---------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------|-------------------------------------------------|
| Tom     | `- nb_sample: 268` <br>`- train : 214 samples`<br>`- test : 54 samples`<br>`- features :`<br>`dominant_color_r, dominant_color_g, dominant_color_b`<br>`- Target : `<br>`black, blue, cyan, green, magenta, red, yellow` | ![](readme_assets/confusion_mtx_1.png) | `- Score: .833334` <br> `- Precision : .879956` |
| Orel    | `- nb_sample: 88` <br>`- train : 70 samples`<br>`- test : 18 samples`<br>`- features :`<br>`dominant_color_r, dominant_color_g, dominant_color_b`<br>`- Target : `<br>`black, blue, cyan, green, magenta, red, yellow`   | ![](readme_assets/confusion_mtx_2.png) | `- Score: .388889` <br> `- Precision : .246913` |
| Stephen | `- nb_sample: 62` <br>`- train : 49 samples`<br>`- test : 13 samples`<br>`- features :`<br>`dominant_color_r, dominant_color_g, dominant_color_b`<br>`- Target : `<br>`black, blue, cyan, green, magenta, red, yellow`   | ![](readme_assets/confusion_mtx_3.png) | `- Score: .615384` <br> `- Precision : .675213` |


#### État final & démonstration

Finalement, nos déroulons le processus complet sur l'ensemble de nos données. Ajout des métriques Precision, Recall, F1Score.

Succinctement : un ia médicale vise à détecter ABSOLUMENT TOUS LES MALADES, 
on cherche à maximiser le recall, et lorque celle-ci prédit un malade, 
alors une précision elevée garantit une prediction fiable. 
Le F1Score (*entre `0` et `1`, FScore = `1/2` => 1 prédiction positive correcte, le modèle fait deux erreurs 
(faux négatif ou faux positif)*) est la moyenne harmonique de la precision et du recall et permet d'avoir une métrique moins biaisée.

```txt
        f1_score = 2 x (recall x precision / recall + precision)
```

Evaluation du dataset complet

|              | Diagramme                                                         |   |
|--------------|-------------------------------------------------------------------|---|
| Données      | ![](readme_assets/all_dataset/all_dataset_data_visualization.gif) |   |
| Repartitions | ![](readme_assets/all_dataset/all_dataset_target_histo.png)       |   |
| Statistiques | ![](readme_assets/all_dataset/all_dataset_rbg_over_target.png)    |   |


| Dataset | Paramètres                                                                                                                                                                                                               | Matrice de confusion                                         | Métriques                                                                                                                                  |
|---------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|
| Complet | `- nb_sample: 418` <br>`- train : 334 samples`<br>`- test : 84 samples`<br>`- features :`<br>`dominant_color_r, dominant_color_g, dominant_color_b`<br>`- Target : `<br>`black, blue, cyan, green, magenta, red, yellow` | ![](readme_assets/all_dataset/all_dataset_confusion_mtx.png) | `- Score: 0.82142857142857` <br> `- Precision : 0.8347436815481928`<br> `- Recall : .8214285714285714`<br> `- F1 Score : .809083004573200` |


### <ins>Retrospective :</ins>

- [ ] ce que j'ai compris
- [ ] ce que je n'ai pas compris
- [ ] les évolutions possibles

```txt
    Nettoyer les images sources, en prelever de nouvelles dans de nouvelles conditions
    Nettoyer et normaliser le dataset
    Prelever d'autres features (donc coder d'autres algos :^) )
    Comparer des kernel SVM (courbe ROC)
    Comparer à un knn (courbe ROC)
```


