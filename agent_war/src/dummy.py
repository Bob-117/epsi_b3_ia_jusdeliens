# import numpy as np
from abc import ABC

from agent_war.base import pytactx


##
class SoldierState(ABC):
    def update_state(self):
        pass


class Watch(SoldierState):
    def update_state(self):
        print('watch')


class Search(SoldierState):
    def update_state(self):
        print('search')


##

class SoldierStateUpdater(ABC):
    def update_state(self):
        pass


class StartWatching(SoldierStateUpdater):
    def update_state(self):
        print('watch')


class StartSearching(SoldierStateUpdater):
    def update_state(self):
        print('search')


##

class Fight(SoldierStateUpdater):
    def update_state(self):
        print('fight')


class Fighter(pytactx.Agent):

    def __init__(self, name):
        super(Fighter, self).__init__(name)

    def changerCouleur(self, rouge, vert, bleu):
        super().changerCouleur(rouge, vert, bleu)

    def find_corner(self):
        print('corner')
        self.deplacer(10, 10)

    def in_corner(self):
        col = self.jeu['gridColumns']
        raw = self.jeu['gridRows']
        return self.x < col - 4 or self.y < raw - 4

    def spotted(self, state):
        if state == "SPOTTED":
            self.changerCouleur(255, 0, 0)


if __name__ == '__main__':
    dummy_movement = [(4, 4), (5, 5), (6, 5), (5, 4), (4, 4)]
    pos = dummy_movement[0]
    bob = Fighter('bob')

    while bob.vie > 0:
        pos = dummy_movement[dummy_movement.index(pos) + 1]
        print(pos)
        # print(pos)
        bob.deplacer(*pos)
        # bob.changerCouleur(128, 0, 128)
        bob.actualiser()
