import logging
import math
from abc import ABC
from abc import ABC
import time
from abc import ABC

import pygame
from pygame.constants import K_ESCAPE, KEYDOWN, QUIT


class Window:
    title: str = 'War'
    # Colours
    BACKGROUND = (255, 255, 255)
    FULLRED = (255, 0, 0)
    FULLGREEN = (0, 255, 0)
    FULLBLUE = (0, 0, 255)
    YELLOW = (255, 255, 0)
    # Game Setup
    SIZE = 600
    FPS = 60
    fpsClock = pygame.time.Clock()
    WINDOW_WIDTH = 500
    WINDOW_HEIGHT = 500

    def __init__(self):
        pygame.display.set_caption(self.title)
        _size = [self.SIZE, self.SIZE]
        self.screen = pygame.display.set_mode(_size)
        self.clock = pygame.time.Clock()

    def reset(self):
        self.screen.fill(self.BACKGROUND)
        for x in range(0, self.SIZE, int(self.SIZE / 20)):  # 20 = game size ie 20*20
            pygame.draw.line(self.screen, self.YELLOW, (1, x), (600, x), 2)
            pygame.draw.line(self.screen, self.YELLOW, (x, 1), (x, 600), 2)

    def update(self):
        # pygame.display.update()
        # self.fpsClock.tick(self.FPS)
        pygame.display.flip()
        self.clock.tick(30)

    @staticmethod
    def event_handler_stop():
        for event in pygame.event.get():
            return event.key == K_ESCAPE if event.type == KEYDOWN else (event.type == QUIT or event.type == 32787)

    def draw(self, obj) -> None:
        self.reset()
        for soldier in obj:
            print(f'____________________________________{soldier}')
            # pygame.draw.circle(self.screen, self.FULLBLUE, soldier[1][0], soldier[1][1], 1, 1)
            # pygame.draw.circle(surface, color, center, radius, width=0, draw_top_right=None, draw_top_left=None, draw_bottom_left=None, draw_bottom_right=None)
            # pygame.draw.circle(self.screen, self.FULLBLUE, 5, 2, width=0, draw_top_right=None, draw_top_left=None, draw_bottom_left=None, draw_bottom_right=None)
            # pygame.draw.circle(screen, (r, g, b), (x, y), R, w)
            _color = (0, 0, 200)
            if soldier[0] == 'gibson':
                _color = (139, 0, 139)
            if soldier[0] == 'bob':
                _color = (0, 128, 0)
            if soldier[0] == 'BOB':
                _color = (255, 255, 0)
            if soldier[0] == 'BOB!':
                _color = (255, 0, 0)
            pygame.draw.circle(self.screen, _color, (soldier[1][0] * 30 - 15, soldier[1][1] * 30 - 15), 15, 15)
        self.update()

    def loop(self, something):
        while not self.event_handler_stop():
            # do something
            ticks = pygame.time.get_ticks()
            print(f'{ticks * 1e-3:.2f}')
            # print(self.clock.get_time())

            self.draw(something)


# class SoldierState:
#     def __init__(self):
#         self.name = "initial state"
#
#     def watch(self, _soldier):
#         ...
#
# class SoldierState(ABC):
#     def update_state(self):
#         pass

class SoldierState(ABC):
    def __init__(self):
        self.name = ''


class Watch(SoldierState):

    def search(self, _soldier):
        _soldier.state = Search()

    def fight(self, _soldier):
        _soldier.state = Fight()

    def watch(self, _soldier):
        _soldier.state = Watch()


class Search(SoldierState):
    def __init__(self):
        super().__init__()
        self.name = "search"

    def search(self, _soldier):
        _soldier.state = Search()

    def watch(self, _soldier):
        _soldier.state = Watch()

    def fight(self, _soldier):
        _soldier.state = Fight()


class Fight(SoldierState):
    def __init__(self):
        super().__init__()
        self.name = "fight"

    def search(self, _soldier):
        _soldier.state = Search()

    def watch(self, _soldier):
        _soldier.state = Watch()

    def fight(self, _soldier):
        _soldier.state = Search()


class Soldier:
    def __init__(self, _name, _x=1, _y=1):
        self.state = Watch()
        self.name = _name
        self.x = _x
        self.y = _y

    # ## State
    def watch(self):
        self.state.watch(self)

    def search(self):
        self.state.search(self)

    def fight(self):
        self.state.fight(self)

    # ## Actions
    def move(self, _x, _y):
        self.x, self.y = _x, _y

    @staticmethod
    # TODO
    def dist(soldier1, soldier2):
        return math.sqrt(
            (soldier1.x - soldier2.x) ** 2
            +
            (soldier1.y - soldier2.y) ** 2
        )

    # ## State handlers
    def update_state(self, _soldier_map):
        """
        update state depending on some rules
        ATM : the map containing others distance {'gibson': 28.284271247461902}
        < 5 alert -> search state
        < 2 engage fight ->
        """
        # print(_soldier_map)
        for _other, dist in _soldier_map.dist.items():
            if dist < 10:
                self.alert(_other)
                if dist < 4:
                    self.engage_fight(_other)
                else:
                    self.alert(_other)
            else:
                self.back_to_watch()

    def alert(self, _other):
        if self.state.name == 'watch':
            print(f'>>>>>>>>>>>>>>ALERT {_other}')
            self.name = self.name.upper()
        if self.state.name == 'fight':
            print(f'>>>>>>>>>>>>>>ALERT {_other}')
            self.name = self.name.upper()
            if self.name.endswith('!'):
                self.name = self.name[:-1]
        self.search()

    def engage_fight(self, _other):
        if self.state.name == 'search':
            print(f'>>>>>>>>>>>>>>FIGHT {_other}')
            self.name += '!'
        self.fight()

    def back_to_watch(self):
        if self.state.name != 'watch':
            print('>>>>>>>>>>>>>>BACK to watch')
            self.name = self.name.lower()
            if self.name.endswith('!'):
                self.name = self.name[:-1]
        self.watch()

    def __str__(self):
        return f'{self.name} - {self.x}:{self.y} - {self.state.name}'


class SoldierMap:
    def __init__(self, _soldier):
        self.current_soldier = _soldier
        self.dist = {}

    def update_dist(self, _new_dist):
        """
        fill all dist like {'gibson': 28.284271247461902}
        """
        self.dist.update(**_new_dist)

    def __str__(self):
        return f'Current Soldier : {self.current_soldier.name}, new dist : {str(self.dist)}'


class Game:
    """
    game with 2 players bob & gibson
    size : 20x20
    movement : dummy diagonal +1+1 / -1-1
    bob is guard i.e. update soldier_map (to refacto) and self.state.
    """
    SIZE = 20

    bob = Soldier('bob')
    gibson = Soldier('gibson')
    bot_x = Soldier('bot_x')
    bot_y = Soldier('bot_y')

    gibson.x = gibson.y = SIZE
    bot_x.x, bot_x.y = 0, 5
    bot_y.x, bot_y.y = 5, 0

    # PLAYERS = [bob, gibson, bot_y, bot_x]
    PLAYERS = [bob, gibson]
    BLUE = [bob]
    RED = [bot_x, bot_y]

    def turn(self, n):
        print(f'############\n  TURN {n}\n############')
        _KEEP_POS = []
        for _, player in enumerate(self.PLAYERS):
            _current_soldier = player
            print(f'Soldier n {_} is moving : {_current_soldier}')
            # TODO _current_player.move(), but need to test state first so it goes like this :

            # RED team crossing on X or Y
            if _current_soldier in self.RED:
                if _current_soldier.name == 'bot_x':
                    _current_soldier.x += 0.8
                if _current_soldier.name == 'bot_y':
                    _current_soldier.y += 1.2
            # bob & gibson dummy diagonal
            _current_soldier.x, _current_soldier.y = list(
                map(
                    lambda coord: coord % self.SIZE,
                    list(
                        map(lambda coord: coord + 1 if not bool(_ % 2) else coord - 1,
                            [_current_soldier.x, _current_soldier.y])
                    )
                )
            )
            _KEEP_POS.append([_current_soldier.name, [_current_soldier.x, _current_soldier.y]])
            # TODO soldier_map as Soldier attribute (only guards?) OR GAME ATTRIBUTE I GUESS :^)
            _soldier_map = SoldierMap(_current_soldier)
            # TODO def update_all_soldiers_maps
            for other_player in list(filter(lambda _player: _player.name != _current_soldier.name, self.PLAYERS)):
                d = Soldier.dist(_current_soldier, other_player)
                new_dist = {other_player.name: d}
                _soldier_map.update_dist(new_dist)
            # TODO current.player.change_state(others) soldier_map attribute
            if _current_soldier in self.BLUE:
                _current_soldier.update_state(_soldier_map)
        return _KEEP_POS

    def play(self):
        """
        run 20 turn & keep positions history
        """
        print(f'############\n  START\n############')
        win = Window()
        time.sleep(3)
        _ALL_POS = {}
        for i in range(20):
            current_pos = self.turn(n=i)
            _ALL_POS.update({
                i: current_pos
            })
            win.draw(current_pos)
            # pygame.image.save(win.screen, f"hehe/screenshot{i}.jpeg")  # save for GIF
            time.sleep(1)
        print(f'############\n  END\n############\n')
        self.show_all_pos(_ALL_POS)
        time.sleep(3)

    @staticmethod
    def show_all_pos(_dict_pos):
        """
        show position
        """
        print(f'{"TURN":<6}' + ''.join([f"|{'NAME' :<8} - {'X':<2}/{'Y':<4}" for _, _soldier in _dict_pos.get(0)]))
        print('___________________________________________')
        for turn, _soldiers in _dict_pos.items():
            print(f'{turn:<6}' + ''.join(
                [f"|{_soldier[0] :<8} - {round(_soldier[1][0], 1):<2}/{round(_soldier[1][1], 1):<4}" for _soldier in
                 _soldiers]))


if __name__ == '__main__':
    game = Game()
    game.play()
