import logging
import random

import threading
from time import perf_counter

from agent_war.base.pytactx import Agent


class DummyAgentThread(threading.Thread):
    def __init__(self, thread_id, name, positions, time_limit):
        threading.Thread.__init__(self)
        self.thread_id = thread_id
        self.name = name
        self.positions = positions
        self.time_limit = time_limit

    def run(self):
        logging.info("Starting thread" + self.name)
        connect_dummy_agent(_dummy_id=self.thread_id, _dummy_movement=self.positions, time_limit=dummies_last_for)
        logging.info("Exiting thread" + self.name)


def connect_dummy_agent(_dummy_id, _dummy_movement, time_limit=10):
    """
    connect a dummy Agent with a defined loop movement
    :param _dummy_id: dummy name
    :param _dummy_movement: list of all positions our dummy will follow
    :time_limit: how long will run our dummy
    :return:
    """
    pos = _dummy_movement[0]
    names = [
        "Bob", "Gibson", "Mordecai", "Orel"
    ]
    current_dummy = Agent(names[_dummy_id - 1]) if _dummy_id < 5 else Agent(f'Agent-{random.randint(0, 100)}')
    start = perf_counter()
    while current_dummy.vie > 0:
        if pos != (current_dummy.x, current_dummy.y):
            current_dummy.deplacer(*pos)
        else:
            _old = pos
            pos = _dummy_movement[(_dummy_movement.index(pos) - 1) % len(_dummy_movement)]
            logging.info(f'Current Agent {current_dummy.nom} reached {_old}, going now to {pos}')
        logging.debug(f'Current Agent {current_dummy.nom}, pos : ({current_dummy.x}, {current_dummy.y}), target : {pos}')
        current_dummy.actualiser()

        # Exiting the loop after X seconds
        elapsed_time = perf_counter() - start
        if elapsed_time >= time_limit:
            break


if __name__ == '__main__':
    logging.getLogger('__main__')
    logging.basicConfig(level='INFO')

    # Create new threads
    dummies_last_for = 10
    thread1 = DummyAgentThread(1, "Thread-1", positions=[(2, 2), (2, 6), (5, 6), (5, 2)], time_limit=dummies_last_for)
    thread2 = DummyAgentThread(2, "Thread-2", positions=[(5, 2), (9, 9), (4, 9)], time_limit=dummies_last_for)

    others = []
    for i in range(10, 15):
        tmp_thread = DummyAgentThread(i, f"Thread-{i}", positions=[(0, i - 2)], time_limit=dummies_last_for)
        others.append(tmp_thread)

    # Start new Threads
    thread1.start()
    thread2.start()

    # ???
    for t in others:
        t.start()
