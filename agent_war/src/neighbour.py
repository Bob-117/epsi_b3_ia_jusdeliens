import enum
import logging
import math
import pprint


class Neighbour:
    def __init__(self, **kwargs):
        self.name = kwargs['name']
        self.x = kwargs['x']
        self.y = kwargs['y']
        self.life = kwargs['life']
        self.ammo = kwargs['ammo']


class _WEIGHT_SAFE_PROFIL(enum.Enum):
    """
    Standart profil
    """
    DIST = 1
    ENOUGH_AMMO = 1
    ENOUGH_LIFE = 1
    THREAT = 1
    NEED_AMMO = 1
    _SAFE_DIST = 8


class _WEIGHT_RAMBO_PROFIL(enum.Enum):
    """
    Survivor will die for ammo
    """
    DIST = 1
    ENOUGH_AMMO = 10
    ENOUGH_LIFE = 1
    THREAT = 0
    NEED_AMMO = 1
    _SAFE_DIST = 8


class NeighbourhoodEval:
    """
    1. Lister les choix possibles
    2. Calculer un coût pour chaque choix
    3. Sélectionner le choix au coût minimal
    4. Exécuter ce choix
    """

    def __init__(self, agent, profil):
        self.__agent = agent
        self.__profil = profil
        self.__weights = self.set_weight_enum()

    def set_weight_enum(self):
        """
        Set the condition weighting depending on Agent profil
        :return:
        """
        if self.__profil == 'safe':
            return _WEIGHT_SAFE_PROFIL
        if self.__profil == 'survivor':
            return _WEIGHT_RAMBO_PROFIL

    def main_process(self, debug=False):
        """
        main process
        List all choices
        Calculate a score for each
        Return the worth option
        :param debug:
        :return:
        """
        data = [self.__agent.voisins, self.__agent.jeu]
        if debug:
            for _dict in data:
                pprint.PrettyPrinter(indent=4).pprint(_dict)
        try:
            # output example
            # [(0.2, <src.neighbour.Neighbour object at 0x7f59f4b71c60>),
            # (0.5, <src.neighbour.Neighbour object at 0x7f59f4b71d50>),
            # (0.4, <src.neighbour.Neighbour object at 0x7f59f4b71db0>)]
            return min([self.eval(choice) for choice in self.list_choices()], key=lambda choice: choice[0])
        except ValueError:
            logging.debug('no neighbour')
            return -1, None

    def eval(self, _other):
        """
        Evaluate one other agent choice depending on metrics
        :param _other:
        :return:
        """
        metrics = [
            next(self.safe_dist(_other)),
            next(self.enough_ammo(_other)),
            next(self.enough_life(_other)),
            next(self.not_a_threat(_other)),
            next(self.do_i_need_ammo(_other)),
        ]
        all_weight = sum(weight for (metric, weight) in metrics)
        note = sum(metric * weight for (metric, weight) in metrics) / all_weight
        return note, _other

    def list_choices(self):
        for other, other_data in self.__agent.voisins.items():
            # print(f'{other} - {other_data["x"]}:{other_data["y"]}')
            yield Neighbour(name=other, x=other_data["x"], y=other_data["y"],
                            life=other_data["life"], ammo=other_data["ammo"])

    def extract_mandatory_data(self):
        for other, other_data in self.__agent.voisins.items():
            print(other)
            print(other_data['ammo'])
            print(other_data['life'])
            print(other_data['x'], other_data['y'])

    def extract_other_position_dict(self):
        for other, other_data in self.__agent.voisins.items():
            yield {other: {other_data['x'], other_data['y']}}

    def extract_other_position(self):
        for other, other_data in self.__agent.voisins.items():
            yield other_data['x'], other_data['y']

    def dist(self, _other):
        """
        dist between self.__agent and other agent
        :param _other:
        :return:
        """
        yield math.sqrt(
            (self.__agent.x - _other.x) ** 2
            +
            (self.__agent.y - _other.y) ** 2
        )

    # ## METRICS
    def safe_dist(self, _other):
        """
        Dist between self.__agent and other agent is safe
        :param _other: other agent
        :param _safe_dist: safe distance
        :return: agent is in a safe spot regarding distance (bool)
        """
        _safe_dist = 10

        yield 1 if next(self.dist(_other)) > _safe_dist else (
            .5 if 5 < next(self.dist(_other)) < _safe_dist else 0), self.__weights.DIST.value

    def enough_ammo(self, _other):
        """
        I kill if I hit all my bullet

        output :
        - 0 if I have enough bullet
        - .5 if I can kill, but I cant really miss bullet
        - 1 if I cant kill
        :param _other:
        :return:
        """
        yield 0 if self.__agent.munitions > _other.life + 50 else (
            .5 if _other.life < self.__agent.munitions < _other.life + 50 else 0), self.__weights.ENOUGH_AMMO.value

    def enough_life(self, _other):
        """
        I have more life
        :param _other:
        :return:
        """
        yield self.__agent.vie > _other.life + 10, self.__weights.ENOUGH_LIFE.value

    def not_a_threat(self, _other):
        """
        Enemy cant kill me, not enough ammo
        :param _other:
        :return:
        """
        yield self.__agent.vie > _other.ammo + 10, self.__weights.THREAT.value

    def do_i_need_ammo(self, other):
        yield self.__agent.munitions > 50 or other.ammo > 50, self.__weights.NEED_AMMO.value

    # ## METRICS BOOL
    def safe_dist_bool(self, _other, _safe_dist=8):
        """
        Dist between self.__agent and other agent is safe
        :param _other: other agent
        :param _safe_dist: safe distance
        :return: agent is in a safe spot regarding distance (bool)
        """

        yield next(self.dist(_other)) > _safe_dist

    def enough_ammo_bool(self, _other):
        """
        I kill if I hit all my bullet -10
        :param _other:
        :return:
        """
        # TODO
        yield self.__agent.munitions > _other.life + 10

    def enough_life_bool(self, _other):
        """
        I have more life
        :param _other:
        :return:
        """
        yield self.__agent.vie > _other.life + 10

    def not_a_threat_bool(self, _other):
        """
        Enemy cant kill me, not enough ammo
        :param _other:
        :return:
        """
        yield self.__agent.vie > _other.ammo + 10

    def do_i_need_ammo_bool(self, other):
        """
        I have more than 50 bullet
        :param other:
        :return:
        """
        yield self.__agent.munitions < 50

    # ## Working area
    def pos_in_area(self, pos, area):
        """
        A pos x:y is in an specific area
        :param pos:
        :param area:
        :return:
        """
        return area + 1 if pos[0] < 5 else area

    def safe_area(self, split=2):
        """
        Le but ici est d'avoir une matrice [[0, 0], [0, 0]]
        dependant de la dimension du damier, et d'un nombre choisi 'split'
        et de définir quelles sont les zones à risques et les zones plus tranquilles
        :param split:
        :return:
        """
        # 'gridColumns': 11, 'gridRows': 11, 'maxAgents': 20
        # size = [self.__agent.jeu['gridColumns'], ['gridRows']]
        _areas = [[0 for _ in range(split)] for _ in range(split)]
        _positions = list(self.extract_other_position())

        print(_areas)
        print(_positions)
        result = list(
            map(lambda row: list(map(lambda area, pos: self.pos_in_area(pos, area), row, _positions)), _areas))

        #1# result = [
        #     [area + 1 if self.pos_in_area(area, _pos) else area for (row, area) in enumerate(rows)] for (row_index, rows) in enumerate(_areas)
        # ]
        # print('""""""""""""""""""')
        # print(result)
        # print('""""""""""""""""""')

        #2# for _pos in self.extract_other_position():
        #     print(_pos)

        #3# _map = list(map(
        #     lambda other: other + 1 if True else + 0,
        #     _areas))
        # print(_map)

        #4# area[i][j] + 1 if 0 <= <= size[0] % split else area[i][j]

        return result

    def eval_old(self, choice):
        """
        return a dict
        as key we have all our condition boolean
        as value we have the weight (for later training)
        {
            'safe_dist': {False: 1},
            'enough_ammo': {False: 1},
            'enough_life': {False: 1},
            'not_a_threat': {False: 1},
            'do_i_need_ammo': {True: 1}
        }
        :param choice:
        :return:
        """
        return {
            'safe_dist': {next(self.safe_dist(choice)): 1},
            'enough_ammo': {next(self.enough_ammo(choice)): 1},
            'enough_life': {next(self.enough_life(choice)): 1},
            'not_a_threat': {next(self.not_a_threat(choice)): 1},
            'do_i_need_ammo': {next(self.do_i_need_ammo(choice)): 1}
        }