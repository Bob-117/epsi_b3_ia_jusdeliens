# import numpy as np

import pytactx
from _distutils_hack import override


class Fighter(pytactx.Agent):

    def __init__(self, name):
        super(Fighter, self).__init__(name)

    def changerCouleur(self, rouge, vert, bleu):
        super().changerCouleur(rouge, vert, bleu)

    def find_corner(self):
        print('corner')
        self.deplacer(10, 10)

    def in_corner(self):
        col = self.jeu['gridColumns']
        raw = self.jeu['gridRows']
        return self.x < col - 4 or self.y < raw - 4

    def spotted(self, state):
        if state == "SPOTTED":
            self.changerCouleur(255, 0, 0)


if __name__ == '__main__':

    bob = Fighter('bob')
    while bob.vie > 0:
        # bob.orienter((bob.orientation + 1) % 4)
        # color = list(np.random.choice(range(256), size=3))
        # color = list(np.random.choice(range(256), size=3))
        bob.changerCouleur(128, 0, 128)

        if bob.in_corner():
            bob.find_corner()

        for voisin in bob.voisins.items():
            print(voisin[0])

            # print(voisin)
        # if bob.jeu

        bob.actualiser()
