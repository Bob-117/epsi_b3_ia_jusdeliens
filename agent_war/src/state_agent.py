from abc import abstractmethod
from enum import Enum
import random

from agent_war.base import pytactx
from agent_war.base import final_state_machine as fsm
from neighbour import NeighbourhoodEval


class StateEnum(Enum):
    """ An Enum is a classy way to put a name on a value"""
    PATROL = 0  # "PatrolState"
    FOLLOW = 1  # "FollowState"
    INVESTIGATE = 2  # "InvestigateState"


class BaseState(fsm.State):
    def __init__(self, agent, color):
        self._agent = agent
        self._color = color

    def handle(self):
        # ADD common behaviour to execute whatever the state
        r, g, b = self._color
        self._agent.changerCouleur(r, g, b)
        # self._agent.tirer((self._agent.distance != 0))
        self._agent.tirer(False)
        self.onHandle()

    @abstractmethod
    def onHandle(self):
        ...


class PatrolState(BaseState):
    """
    Specific State that should send signals to StateMachine
    Implement handle() behavior
    """

    def __init__(self, agent):
        super().__init__(agent, (0, 255, 0))

    def onHandle(self):
        # if agent detected
        if self._agent.distance != 0:
            # switch to FollowState
            self.switch_state(StateEnum.FOLLOW.value)  # en python, self est obligatoire
        # otherwise
        else:
            # Move agent around a patrol point
            self._agent.deplacer(self._agent.x + random.randint(-1, 1),
                                 self._agent.y)
            self._agent.orienter((self._agent.orientation + 1) % 4)


class FollowState(BaseState):
    def __init__(self, agent):
        super().__init__(agent, (255, 0, 0))
        self.__xEnnemy = 0
        self.__yEnnemy = 0

    def onHandle(self):
        # If agent on sight, save location
        if self._agent.distance != 0:
            if self._agent.orientation == 0:
                self.__xEnnemy = self._agent.x + self._agent.distance
                self.__yEnnemy = self._agent.y
            elif self._agent.orientation == 1:
                self.__xEnnemy = self._agent.x
                self.__yEnnemy = self._agent.y - self._agent.distance
            elif self._agent.orientation == 2:
                self.__xEnnemy = self._agent.x - self._agent.distance
                self.__yEnnemy = self._agent.y
            elif self._agent.orientation == 3:
                self.__xEnnemy = self._agent.x
                self.__yEnnemy = self._agent.y + self._agent.distance
        # If not arrived at the last known location of the ennemy
        # isArrived = ( self.x == self.__xEnnemy and self.y == self.__yEnnemy )
        # isNotArrived = not(self.x == self.__xEnnemy and self.y = self.__yEnnemy)
        # isNotArrived = (self.x != self.__xEnnemy or self.y != self.__yEnnemy)
        if not (self._agent.x == self.__xEnnemy and self._agent.y == self.__yEnnemy):
            self._agent.deplacer(self.__xEnnemy, self.__yEnnemy)
        else:  # Enemy not on site anymore
            # Investigate
            self.switch_state(StateEnum.INVESTIGATE.value)


class InvestigateState(BaseState):
    def __init__(self, agent):
        super().__init__(agent, (255, 128, 0))

    def onHandle(self):
        """
        An Agent walks around in the reseach area
        """
        # if agent not dead
        if self._agent.distance != 0:
            # fire
            self._agent.tirer(True)
        # otherwise
        else:
            # Move agent around a patrol point
            self.switch_state(StateEnum.PATROL.value)


class StateMachineAgent(pytactx.Agent):
    def __init__(self, myId):
        # Our data handler evaluating some metrics to define the best choice
        self.neighbour_handler = NeighbourhoodEval(self, profil='safe')

        # create state machine
        self.__state_machine = fsm.StateMachine()
        # register all states depending on pytactx context
        # it uses append(), the order matter
        self.__state_machine.add_state(PatrolState(self))
        self.__state_machine.add_state(FollowState(self))
        self.__state_machine.add_state(InvestigateState(self))
        # set the initial state depending on pytactx context
        self.__state_machine.set_actual_state(StateEnum.PATROL.value)
        # finally the super init
        _pwd = input('pass ? ')
        super().__init__(id=myId,
                         username="demo",
                         password=_pwd,
                         arena="pytactx",
                         server="mqtt.jusdeliens.com",
                         prompt=False,
                         verbose=False)

    def quandActualiser(self):
        best_option = self.neighbour_handler.main_process()
        if len(best_option) == 2:
            if best_option[0] != -1:
                score, best_choice = best_option
                print(f'Best choice : {best_choice.name} with {score}/1 score, attack on {best_choice.x}:{best_choice.y}')

        # print(f'Best choice : {best_choice.name} with {score}/1 score, attack on {best_choice.x}:{best_choice.y}'
        #       if best_choice != -1
        #       else 'pas de choix')
        self.__state_machine.handle()
