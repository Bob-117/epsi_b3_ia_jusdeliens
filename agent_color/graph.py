import pandas as pd
from matplotlib import pyplot as plt

df = pd.read_csv("dataset.csv")

colors = ["red", "blue", "cyan", "yellow", "green", "magenta"]
color_stats = []
for c in colors:
    subset = df[df["target"] == c]
    r_mean = subset["dominant_color_r"].mean()
    g_mean = subset["dominant_color_g"].mean()
    b_mean = subset["dominant_color_b"].mean()
    color_stats.append((r_mean, g_mean, b_mean))

color_stats = pd.DataFrame(color_stats, columns=["R", "G", "B"], index=colors)
color_stats.plot(kind="bar")
plt.xlabel("Target color")
plt.ylabel("Average R, G, B")
plt.show()
