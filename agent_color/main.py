import src.frame_process as frame_process
import src.ia_process as ia_process


def process():

    # mode = input('Mode ?')
    mode = '2'

    if mode in ['0', 'data_gathering']:
        from agent_color.src import Picasso

        _id = 'ova1097bdcca1cd'
        _user_name = '🎸Bob117'
        picasso_agent = Picasso(ova_id=_id, _username=_user_name)
        frame_process.gathering(_agent=picasso_agent, time_between_frames=2, nb_frames=200,
                                known_colors=['noir', 'rouge', 'jaune', 'vert', 'cyan', 'bleu'])
    elif mode in ['1', 'generate_dataset']:
        # for _ in ['tom', 'stephen', 'orel']:
        #     frame_process.loop_on_pictures_from_src(src=_)
        frame_process.loop_on_pictures_from_src(src='stephen')
    elif mode in ['2', 'train']:
        ia_process.train_process('frames')
    elif mode in ['3', 'predict']:
        ia_process.predict_process()
    else:
        print('nope')


if __name__ == '__main__':
    process()
