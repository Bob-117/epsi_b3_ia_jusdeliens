from agent_color.src.svm_model import SVMModel


def train_process(src):
    print('training process')

    _svm_model = SVMModel(dataset=f'dataset/{src}.csv')
    _svm_model.train()


def predict_process():
    print('predict process')
