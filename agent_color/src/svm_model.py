# Standard lib
import logging
from abc import ABC

# Maths lib
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from numpy import array

# AI lib
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, precision_score, accuracy_score, recall_score, f1_score

# Custom lib
import agent_color.src.data_visualization as visu


class SVMModel(ABC):

    def __init__(self, dataset):
        # initial training dataset & classifier
        self.dataset = dataset
        self.classifier = SVC()
        self.features_name = []
        self.target_name = []
        # training data
        self.__data = None
        self.__features = None
        self.__target = None
        self.__feature_train = None
        self.__feature_test = None
        self.__target_train = None
        self.__target_test = None
        # validation
        self.__confusion = None
        self.__recall = 0
        self.__precision = 0
        self.__f1_score = 0

    # ## TRAIN CLASSIFIER ##############

    def __load_data_from_csv(self):
        """
        load dataset from csv file
        :return:
        """
        print(f'loading data from {self.dataset}')
        data = pd.read_csv(self.dataset)
        self.__data = data
        self.features_name = list(data.keys())
        (_feature, _target) = (data.iloc[:, :-1].values, data.iloc[:, -1].values)
        self.target_name = np.unique(_target)
        self.__features, self.__target = _feature, _target.reshape(len(_target), 1)  # always handle m*n array
        print(f'Loaded : {len(self.__features)} with features {self.features_name}, Target : {self.target_name}')
        return self.__features, self.__target

    def __split_data(self, test_size=.2):
        """
        split dataset between :
        - feature_train ie x_train
        - target_train ie y_train
        - feature_test ie x_test
        - target_train ie y_test
        """
        _entry_feature, _entry_target = self.__features, self.__target
        self.__feature_train, self.__feature_test, self.__target_train, self.__target_test = train_test_split(
            _entry_feature, _entry_target, test_size=test_size
        )
        return [self.__feature_train, self.__feature_test, self.__target_train, self.__target_test]

    def __display_data(self):
        """

        """
        visu.plot_3d(axes=self.features_name, _x=self.__features, _y=self.__target)
        # visu.plot_histogramme(_df=self.__data)
        # visu.plot_stats_rbg(_df=self.__data)

    def train(self):
        """
        split data (x_train, y_train, x_test, y_test)
        train model with classifier.fit(x_train, y_train)
        display 2d or 3d graph depending on features number (x)
        :return:
        """
        self.__load_data_from_csv()
        self.__display_data()

        self.__split_data()
        self.classifier.fit(self.__feature_train, self.__target_train.reshape(len(self.__target_train, )))
        score = self.classifier.score(self.__feature_test, self.__target_test)
        print(f'Score de {score}, {"Success training" if score > .8 else "Failed training => need more/better data"}')

        # # Try this to get the best training set up
        # while score < .8:
        #     print(f'again : {score}')
        #     try:
        #         self.__split_data()
        #         self.classifier.fit(self.__feature_train, self.__target_train.reshape(len(self.__target_train, )))
        #         self.score = self.classifier.score(self.__feature_test, self.__target_test)
        #     except ValueError:
        #         print('nope')
        #         continue

        self.metrics()

        self.plot_confusion_matrix()
        self.__generate_graph()
        return self.classifier

    # ## MANAGE CLASSIFIER ##############

    def save(self):
        """
        pickle.dump()
        :return:
        """
        pass

    def reload_model(self):
        """
        pickle.load()
        :return:
        """
        pass

    # ## VALIDATE CLASSIFIER ##############

    def confusion_matrix(self):
        self.__confusion = confusion_matrix(self.__target_test, self.classifier.predict(self.__feature_test))
        return self.__confusion

    def custom_confusion(self):
        """
        True Positive TP
        False Positive FP
        True Negative TN
        False Negative FN

        Positive = TP + FN
        Negative = TN + FP

        TP_rate = TP / (TP + FN) = TP / Positive
        FP_rate = FP / (FP + TN) = FP / Negative
        TN_rate = TN / (TN + FP) = TN / Negative
        FN_rate = FN / (FN + TP) = FN / Positive

        good classifier = high TP & TN
        [[10, 1],
        [1, 10]]
        good classifier = high TP_rate, high TN_rate, low FP_rate & low FN_rate
        """
        if len(np.unique(self.__target)) == 2:  # If targets in {0, 1}
            TP = FP = TN = FN = 0
            for y, y_hat in zip(self.__target_test, self.classifier.predict(self.__feature_test)):
                if y == 1 and y_hat == 1:
                    TP += 1
                elif y == 0 and y_hat == 0:
                    TN += 1
                elif y == 1 and y_hat == 0:
                    FN += 1
                elif y == 0 and y_hat == 1:
                    FP += 1

            return np.array([
                [TN, FP],
                [FN, TP]
            ])
        else:
            logging.info("More than 2 unique targets, using sklearn confusion method")
            return self.confusion_matrix()

    def plot_confusion_matrix(self):
        """
        Plot the confusion matrix after training
        """
        fig, ax = plt.subplots()
        im = ax.imshow(self.__confusion, cmap='Blues')

        ax.figure.colorbar(im, ax=ax)

        ax.set(xticks=np.arange(self.__confusion.shape[1]),
               yticks=np.arange(self.__confusion.shape[0]),
               xticklabels=self.target_name, yticklabels=self.target_name,
               title='Confusion matrix')

        # rotate the x-axis labels, fun one
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
                 rotation_mode="anchor")

        for i in range(self.__confusion.shape[0]):
            for j in range(self.__confusion.shape[1]):
                ax.text(j, i, format(self.__confusion[i, j], 'd'),
                        ha="center", va="center",
                        color="white" if self.__confusion[i, j] > self.__confusion.max() / 2. else "black")

        # plt.savefig('confusion_matrix.png')
        plt.show()

    def metrics(self):
        accuracy = accuracy_score(self.__target_test, self.classifier.predict(self.__feature_test))
        precision = precision_score(self.__target_test, self.classifier.predict(self.__feature_test),
                                    average='weighted')
        recall = recall_score(self.__target_test, self.classifier.predict(self.__feature_test), average='weighted')
        f1 = f1_score(self.__target_test, self.classifier.predict(self.__feature_test), average='weighted')

        print(f'-----TRAINING RESULTS for {self.dataset}-------')
        print(
            f'nb_sample: {len(self.__features)} | {len(self.__target)}, train : {len(self.__feature_train)} samples, test : {len(self.__feature_test)} samples')
        print(f'conf mtx:{self.confusion_matrix()}')
        print(f"Accuracy: {accuracy}")
        print(f"Precision: {precision}")
        print(f"Recall: {recall}")
        print(f"F1-score: {f1}")

    def precision(self):
        return precision_score(self.__target_test, self.classifier.predict(self.__feature_test), average='weighted')

    def precision_recall_score(self):
        """
        Precision = 0 < TP / (TP + FP) < 1
        for all points declared positive, what percentage are actually positive

        Recall = 0 < TP / (TP + FN) = TP / Positive < 1
        for all points actually positive, what percentage was the model able to detect/predict
        """
        pass

    def f1_score(self):
        """
        f1_score = 0 < 2 * (precision * recall) / (precision + recall) < 1
        higher = better
        :return:
        """
        pass

    # ## VISUALIZE CLASSIFIER ##############

    def __generate_graph(self):
        ...

    # ## PREDICT WITH A TRAINED CLASSIFIER ##############

    def predict(self, data):
        """
        target_predict = model.predict(data) = [0] | [1]
        based on previous training
        :param data:
        :return:
        """
        return self.classifier.predict(
            array([data])
        )

    def predict_process(self, data):
        """
        Run the IA prediction on a new data entry
        :param data:
        :return:
        """
        print(f'-----RUNNING PREDICTION for {data} '
              f'based on {self.dataset} '
              f'training (model : {self.classifier})-------')
        result = self.predict(data)
        print(f'We estimate {data} to be target {result[0]} with {self.precision()}e2%')
        return result[0]
