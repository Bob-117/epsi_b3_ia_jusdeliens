"""
Module with some matplotlib methods to visualize Machine Learning Dataset
"""
import imageio
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt


def plot_3d(axes, _x, _y):
    """
    Plot 3 features 1 color target as matplotlib 3d
    """
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    ax.set_xlabel(axes[0])
    ax.set_ylabel(axes[1])
    ax.set_zlabel(axes[2])

    for var, target in zip(_x, _y):
        ax.scatter(var[0], var[1], var[2], color=target[0], label=target[0], zorder=1)

    save_sample_png_for_gif(_fig=fig, _ax=ax)

    plt.title("DATASET VISUALIZATION")

    plt.show()
    return plt


def plot_histogramme(_df):
    """
    Plot histogramme target dispersion
    """

    colors = ["red", "blue", "cyan", "yellow", "green", "magenta", "black"]
    color_counts = [_df[_df["target"] == c].shape[0] for c in colors]
    plt.bar(colors, color_counts, color=colors)
    plt.xlabel("Color")
    plt.ylabel("Count")
    plt.show()


def plot_stats_rbg(_df):
    """
    Plot RGB means for each color target
    """
    colors = ["red", "blue", "cyan", "yellow", "green", "magenta"]
    color_stats = []
    for c in colors:
        subset = _df[_df["target"] == c]
        r_mean = subset["dominant_color_r"].mean()
        g_mean = subset["dominant_color_g"].mean()
        b_mean = subset["dominant_color_b"].mean()
        color_stats.append((r_mean, g_mean, b_mean))

    color_stats = pd.DataFrame(color_stats, columns=["R", "G", "B"], index=colors)
    color_stats.plot(kind="bar", color=['red', 'green', 'blue'])
    plt.xlabel("Color")
    plt.ylabel("Average R, G, B")
    plt.show()


def save_sample_png_for_gif(_fig, _ax):
    """
    Save X rotating matplotlib frames to generate a gif in src/gif
    """
    with imageio.get_writer('all_dataset.gif', mode='I') as writer:
        for angle in range(0, 360, 1):  # change framerate
            print(angle)
            _ax.view_init(20, angle)
            plt.savefig(f'gif/sample/sample_{angle}')
            image = np.frombuffer(_fig.canvas.tostring_rgb(), dtype='uint8')
            image = image.reshape(_fig.canvas.get_width_height()[::-1] + (3,))
            writer.append_data(image)
