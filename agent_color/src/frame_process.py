import itertools
from collections import Counter
from time import sleep
import numpy as np
import os
import pickle

from PIL import Image
from matplotlib import pyplot as plt
from sklearn.cluster import KMeans
from kneed import KneeLocator

from agent_color.src import Picasso


# ## FROM PICTURES TO FRAMES

def gathering(_agent: Picasso, time_between_frames, nb_frames, known_colors):
    """
    Frame gathering process with an OVA Agent (Pytactx)
    Save as png & pixels_matrix.pickle
    """
    print(f'Starting data gathering on {nb_frames} frames, waiting {time_between_frames} between each frame')

    for i in range(nb_frames):
        print(f'STEP {i}')
        current_color = known_colors[i % len(known_colors)]

        # Update the Agent
        _agent.update()

        # Get the current frame
        current_frame = _agent.handle_one_frame()

        # Split generator for further handling
        current_frame_to_save, current_frame_to_parse = itertools.tee([(list(_)) for _ in current_frame], 2)

        # Sleep before next irl color
        sleep(time_between_frames)

        # Save result
        save_results(color=current_color, frame=current_frame_to_save, step=i)


def rgb_px_array_to_pillow(_px_array, path, file_name):
    """
    Save a matrix
    [
        [[rgb], [rgb]],
        [[rgb], [rgb]]
    ]
    as a pillow image for further handling
    """
    print(f'Saving pillow in {path}/{file_name}')

    # Convert the pixels into an array using numpy
    array = np.array([(list(_)) for _ in _px_array], dtype=np.uint8)

    # Use PIL to create an image from the new array of pixels
    new_image = Image.fromarray(array)

    # Save image
    if not os.path.exists(path):
        print(f'creating {path}')
        os.mkdir(path)
    img_save = f'{path}/{file_name}.png'
    new_image.save(img_save)


def save_px_array(px_array, pickle_directory, pickle_file):
    """
    Save a matrix
    [
        [[rgb], [rgb]],
        [[rgb], [rgb]]
    ]
    as a pickle model for further handling
    """

    print(f'Saving pickle in {pickle_directory}/{pickle_file}')

    if not os.path.exists(pickle_directory):
        print(f'creating {pickle_directory}')
        os.mkdir(pickle_directory)

    with open(os.path.join(pickle_directory, pickle_file), 'wb+') as handle:
        pickle.dump([(list(_)) for _ in px_array], handle, protocol=pickle.HIGHEST_PROTOCOL)


def save_results(color, frame, step):
    """
    gathering data to generate a dataset
    for now, saving img with pillow, and rbg matrix with pickle
    """
    print('save result')
    frame_to_pillow, frame_to_array = itertools.tee([(list(_)) for _ in frame], 2)

    rgb_px_array_to_pillow(frame_to_pillow, path=f'dataset/img/{color}', file_name=f'my_img{step}')
    save_px_array(px_array=frame_to_array, pickle_directory=f'dataset/pickle/{color}', pickle_file=f'{step}.pickle')


# ## FROM FRAMES TO FEATURES

def loop_on_pictures_from_src(src):
    """
    Loop on several pictures file
    Ine the directory dataset/data/data_{src}/captured
    e.g. dataset/data/data_tom/captured/blue/1.png
    Extract mandatory features & write it in a csv file to train a model
    """
    captured_directory = f'dataset/data/data_{src}/captured'
    print(f'SRC : {src}')
    for subdir, dirs, files in os.walk(captured_directory):
        if files:
            target = os.path.basename(subdir)
            count = 0
            for file in files:
                count += 1
                print(f'>>> {src} >>> {target} >>> {file}')
                if count < 200:
                    one_frame_to_csv_line(os.path.join(subdir, file), target=target, _n=count, src=src)


def one_frame_to_csv_line(frame, target, src, _n):
    """
    EXTRACT FEATURE HERE, FORMAT ROW TO CSV
    """
    dominant_colors = elbow_method_colors_clustering(frame, target, src, _n)
    # print(f'CSV ROW : {dominant_colors}')

    with open(f'dataset/{src}.csv', 'a+') as final_dataset:
        print(f'One more row to {final_dataset}')
        color = dominant_colors[1]
        r1, g1, b1 = color[0], color[1], color[2]
        final_dataset.write(f'{r1},{g1},{b1},{target}\n')


def elbow_method_colors_clustering(frame, target, src, _n):
    """
    Main method
    Find n* the optimal clusters number
    Return the n* dominant colors
    """
    # Load the image
    img = Image.open(frame)
    img = img.convert('RGB')

    # Convert image to nparray
    img_array = np.array(img)

    # Flatten the array : rows of pixels
    rows, cols, ch = img_array.shape
    pixels = img_array.reshape(rows * cols, ch)

    # TODO params + test perf
    cluster_max = 12

    # Elbow method to find the best number of clusters
    disp = []
    for k in range(1, cluster_max):
        kmeans = KMeans(n_clusters=k, random_state=0, n_init='auto').fit(pixels)
        disp.append(kmeans.inertia_)

    # ## PLOT ELBOW
    # plt.plot(range(1, cluster_max), disp)
    # plt.title('Elbow Method')
    # plt.xlabel('Number of clusters')
    # plt.ylabel('Inertia | Dispersion')
    # plt.show()

    # Find the optimal number of clusters
    kn = KneeLocator(range(1, cluster_max), disp, curve='convex', direction='decreasing')
    opti_n_clusters = kn.knee
    print('N : ', opti_n_clusters)

    # Perform k-means clustering with the best number of clusters
    kmeans = KMeans(n_clusters=opti_n_clusters, random_state=0, n_init='auto').fit(pixels)

    # Get the dominant colors & frequencies
    labels = kmeans.labels_
    dominant_colors = kmeans.cluster_centers_

    # Normalize colors [0, 1]
    dominant_colors_norm = dominant_colors / 255
    # print('DOMINANT COLORS : ', dominant_colors_norm)

    # HANDLING SOME 1.000000005856 ????? fck u
    norm_dom_color = list(
        map(
            lambda row: list(
                map(lambda value: 1 if value > 1 else value,
                    row
                    )
            ), dominant_colors_norm)
    )

    color_counts = Counter(labels)

    debug = False
    if debug:
        print('DOMINANT COLORS : ', dominant_colors.astype(int))
        print('COLOR COUNT', color_counts)
        print('COLOR COUNT VALUES', color_counts.values())
        print('NORM_COM_COLOR', norm_dom_color)

    # Dominant colors bar plot
    plt.bar(range(len(color_counts)), color_counts.values(), color=norm_dom_color)
    plt.xticks(range(len(color_counts)), dominant_colors.astype(int))
    plt.xlabel('Dominant Colors')
    plt.ylabel('Frequency')

    _save = False
    if _save:
        print('saving')
        plt.savefig(f'../readme_assets/histo/{src}/{target}_{_n}.png')

    _show = False
    if _show:
        plt.show()

    plt.close()
    return dominant_colors.astype(int)
