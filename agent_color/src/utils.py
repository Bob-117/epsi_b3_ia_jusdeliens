import pickle


def load_pickle(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


if __name__ == '__main__':
    res = load_pickle('../dataset/pickle/194.pickle')
    print(res)