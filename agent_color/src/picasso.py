from agent_color.base.ova import OvaClientMqtt


class Picasso(OvaClientMqtt):

    def __init__(self, ova_id, _username):
        self.current_img = None
        _pwd = input('pass ? ')
        super().__init__(id=ova_id,
                         username=_username,
                         password=_pwd,
                         arena="ishihara",
                         server="192.168.10.103",
                         imgOutputPath=None)

    def handle_one_frame(self):
        """
        Get one real picture
        Return pixels matrix like
        [
            [[r, g, b],[r, g, b],[r, g, b]],
            [[r, g, b],[r, g, b],[r, g, b]],
            [[r, g, b],[r, g, b],[r, g, b]],
        ]
        """
        if self.getImageWidth() == 0 or self.getImageHeight() == 0:
            print('ERROR FETCHING IMAGES')
            exit(117)

        width, height = self.getImageWidth(), self.getImageHeight()
        px_rgb = (
            (
                list(
                    map(lambda rgb: rgb, self.getImagePixelRGB(x, y))
                )
                for x in range(width))
            for y in range(height)
        )
        self.current_img = px_rgb
        return px_rgb
