import glob
from PIL import Image
from PIL import Image, ImageSequence


def gif_1():
    """
    Mieux avec pygame
    """
    frames = []
    imgs = glob.glob("sample/*.png")
    print(len(imgs))
    for index, i in enumerate(imgs):
        # if index % 2 == 0:
        new_frame = Image.open(i)
        frames.append(new_frame)

    # Save into a GIF file that loops forever
    frames[0].save('data_visualization.gif', format='GIF',
                   append_images=frames[1:],
                   save_all=True,
                   duration=720, loop=0)


def gif_2():
    """
    op
    """
    frames = []
    for i in range(360):
        filename = f"sample/sample_{i}.png"
        image = Image.open(filename)
        frames.append(image)
    frames[0].save("data_visualization.gif", save_all=True, append_images=frames[1:], duration=50, loop=0)


if __name__ == '__main__':
    gif_2()